/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Frank.Widgets.Model;
using Internationalization;

namespace Frank.Widgets
{
    /// <summary>
    /// Cell renderer that uses a ComboBox widget with enum values for editing.
    /// </summary>
    public class CellRendererComboEnum<T>: Gtk.CellRendererCombo
        where T: struct
    {
        /// <summary>Delegate method for EnumValueEdited event.</summary>
        public delegate void EnumValueEditedDelegate(object o, Gtk.EditedArgs args, T enumValue);
        /// <summary>
        /// Occurs when enum value was edited.
        /// </summary>
        public event EnumValueEditedDelegate EnumValueEdited;

        private T enumValue;
        private TranslatableEnumListStore<T> store;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.CellRendererComboEnum`1"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="domain">I18n domain.</param>
        /// <param name="includeExperimental">If set to <c>true</c> include enum values that are marked as experimental
        /// (<see cref="ExperimentalAttribute"/>.</param>
        public CellRendererComboEnum (I18n i18n, string domain, bool includeExperimental)
        {
            store = new TranslatableEnumListStore<T>(i18n, domain, includeExperimental);
            Model = store;
            TextColumn = TranslatableEnumListStore<T>.NameStringColumn;
            HasEntry = false;
            Edited += HandleEdited;
        }

        private void HandleEdited (object o, Gtk.EditedArgs args)
        {
            if (EnumValueEdited != null) {
                foreach (GenericListRow<string, TranslationString, T> row in store) {
                    if (row.Item1 == args.NewText) {
                        EnumValueEdited(o, args, row.Item3);
                    }
                }
            }
        }

        /// <inheritdoc/>
        public override Gtk.CellEditable StartEditing (Gdk.Event evnt, Gtk.Widget widget, string path, Gdk.Rectangle background_area, Gdk.Rectangle cell_area, Gtk.CellRendererState flags)
        {
            Gtk.ComboBox comboBox = base.StartEditing (evnt, widget, path, background_area, cell_area, flags) as Gtk.ComboBox;
            Gtk.TreeIter iter = store.GetIter (enumValue);
            comboBox.SetActiveIter(iter);
            return comboBox;
        }

        /// <summary>
        /// Gets or sets the enum value.
        /// </summary>
        /// <value>The enum value.</value>
        [GLib.Property("enum-value")]
        public T EnumValue {
            get {
                return enumValue;
            }
            set {
                this.enumValue = value;
                Gtk.TreeIter iter = store.GetIter(value);
                Text = store[iter].Item1;
            }
        }
    }
}

