/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Widgets
{
    /// <summary>
    /// Cell renderer that renders a <c>Cairo.Color</c>.
    /// </summary>
    public class CellRendererColor: Gtk.CellRenderer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.CellRendererColor"/> class.
        /// </summary>
        public CellRendererColor ()
        {
            CellWidth = 10;
            CellHeight = 10;
        }

        /// <inheritdoc/>
        public override void GetSize (Gtk.Widget widget, ref Gdk.Rectangle cell_area, out int x_offset, out int y_offset, out int width, out int height)
        {
            x_offset = 0;
            y_offset = 0;
            width = CellWidth;
            height = CellHeight;
        }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>The color.</value>
        [GLib.Property("color")]
        public Cairo.Color Color {
            set;
            get;
        }

        /// <summary>
        /// Gets or sets the width of the cell.
        /// </summary>
        /// <value>The width of the cell.</value>
        public int CellWidth {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the height of the cell.
        /// </summary>
        /// <value>The height of the cell.</value>
        public int CellHeight {
            get;
            set;
        }

        /// <inheritdoc/>
        protected override void Render (Gdk.Drawable window, Gtk.Widget widget, Gdk.Rectangle background_area, Gdk.Rectangle cell_area, Gdk.Rectangle expose_area, Gtk.CellRendererState flags)
        {
            Cairo.Context context = Gdk.CairoHelper.Create(window);
            context.Color = Color;
            context.Rectangle (cell_area.X, cell_area.Y, cell_area.Width, cell_area.Height);
            context.Fill ();
            (context as IDisposable).Dispose();
        }
    }
}

