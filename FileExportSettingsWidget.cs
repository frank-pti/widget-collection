/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using InternationalizationUi;
using Internationalization;

namespace Frank.Widgets
{
    /// <summary>
    /// Widget for file export settings.
    /// </summary>
	public class FileExportSettingsWidget: Gtk.VBox
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FileExportSettingsWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="curveDataAvailable">If set to <c>true</c> curve data is available.</param>
        /// <param name="includeCurveData">If set to <c>true</c> include curve data.</param>
        /// <param name="openAfterSave">If set to <c>true</c> open after save.</param>
		public FileExportSettingsWidget(I18n i18n, bool curveDataAvailable, bool includeCurveData, bool openAfterSave)
		{
			IncludeCurveDataCheckButton = new TranslatableCheckButton(i18n, Constants.UiDomain, "Include curve data") {
				Active = includeCurveData,
				Sensitive = curveDataAvailable
			};
			Add(IncludeCurveDataCheckButton);

			OpenAfterSaveCheckButton = new TranslatableCheckButton(i18n, Constants.UiDomain, "Open file after saving") {
				Active = openAfterSave
			};
			Add(OpenAfterSaveCheckButton);

			ShowAll();
		}

        /// <summary>
        /// Gets the include curve data check button.
        /// </summary>
        /// <value>The include curve data check button.</value>
        public Gtk.CheckButton IncludeCurveDataCheckButton {
            private set;
            get;
        }

        /// <summary>
        /// Gets the open after save check button.
        /// </summary>
        /// <value>The open after save check button.</value>
        public Gtk.CheckButton OpenAfterSaveCheckButton {
            private set;
            get;
        }
	}
}

