﻿using Frank.Widgets.Model;
using Gtk;
using System.Collections.Generic;

namespace Frank.Widgets
{
    /// <summary>
    /// ComboBox that displays elements from a generic list.
    /// </summary>
    /// <typeparam name="T">The list element type.</typeparam>
    public class ListComboBox<T> : ComboBox
        where T : class
    {
        object[] renderParameters;

        /// <summary>
        /// Initializes a new instance of the <see cref="ListComboBox"/> class.
        /// </summary>
        /// <param name="list">The list to be displayed.</param>
        public ListComboBox(IList<T> list) :
            this(list, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref=" ListComboBox"/> class.
        /// </summary>
        /// <param name="list">The list to be displayed.</param>
        /// <param name="renderParameters">Additional objects needed for building the data string.</param>
        /// <remarks>The <c>renderParameters</c> object will be passed to the <see cref="BuildDataString"/> method
        /// as parameter.</remarks>
        protected ListComboBox(IList<T> list, params object[] renderParameters)
        {
            this.renderParameters = renderParameters;

            GenericListStore<T> listStore = new GenericListStore<T>();
            CellRendererText textRenderer = new CellRendererText();
            PackStart(textRenderer, false);
            SetCellDataFunc(textRenderer, RenderData);
            foreach (T item in list) {
                listStore.AppendValues(item);
            }
            Model = listStore;
            if (list.Count > 0) {
                ActiveItem = list[0];
            }
        }

        private void RenderData(Gtk.CellLayout cellLayout, CellRenderer cell, TreeModel treeModel, TreeIter iter)
        {
            T item = treeModel.GetValue(iter, 0) as T;
            (cell as Gtk.CellRendererText).Text = BuildDataString(item, renderParameters);
        }

        /// <summary>
        /// Build the data string that should be displayed.
        /// </summary>
        /// <param name="item">The item that should be rendered.</param>
        /// <param name="renderParameters">The render parameters (from <see cref="ListComboBox(IList<T>,object[])"/>
        /// constructor.</param>
        /// <returns></returns>
        protected virtual string BuildDataString(T item, object[] renderParameters)
        {
            return item.ToString();
        }

        /// <summary>
        /// Gets or sets the active item.
        /// </summary>
        /// <value>The active item.</value>
        public T ActiveItem
        {
            get
            {
                TreeIter iter;
                if (GetActiveIter(out iter)) {
                    return Model.GetValue(iter, 0) as T;
                }
                return null;
            }
            set
            {
                TreeIter iter;
                Model.GetIterFirst(out iter);
                do {
                    T export = Model.GetValue(iter, 0) as T;
                    if (export != null && export.Equals(value)) {
                        SetActiveIter(iter);
                    }
                } while (Model.IterNext(ref iter));
            }
        }
    }
}
