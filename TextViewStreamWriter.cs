using System;
using System.IO;
using System.Text;
using Gtk;

namespace Frank.Widgets
{
    /// <summary>
    /// Stream writer that writes to a text view.
    /// </summary>
	public class TextViewStreamWriter: TextWriter
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.TextViewStreamWriter"/> class.
		/// </summary>
		public TextViewStreamWriter ():
			this(null, null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.TextViewStreamWriter"/> class.
		/// </summary>
		/// <param name="formatProvider">Format provider.</param>
		public TextViewStreamWriter(IFormatProvider formatProvider):
			this(formatProvider, null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.TextViewStreamWriter"/> class.
		/// </summary>
		/// <param name="output">
		/// A <see cref="TextView"/>, where the text should be written to.
		/// </param>
		public TextViewStreamWriter(TextView output):
			this(null, output)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TextViewStreamWriter"/> class.
		/// </summary>
		/// <param name="formatProvider">Format provider.</param>
		/// <param name="output">A <see cref="TextView"/>, where the text should be written to.</param>
		public TextViewStreamWriter(IFormatProvider formatProvider, TextView output):
			base(formatProvider)
		{
			Output = output;
		}

		#region implemented abstract members of TextWriter
		/// <summary>
		/// Writes a character to the specified <see cref="TextView"/> widget.
		/// </summary>
		/// <param name="value">
		/// The character to write to the text stream.
		/// </param>
		public override void Write (char value)
		{
			TextIter start, end;
			Output.Buffer.GetBounds(out start, out end);
			char[] array = new char[] { value };
			Output.Buffer.Insert(ref end, new String(array));
		}

		/// <summary>
		/// Gets the encoding.
		/// </summary>
		/// <value>The encoding.</value>
		public override Encoding Encoding {
			get {
				return Encoding.UTF8;
			}
		}
		#endregion

		/// <summary>
		/// The output text view where the logging text is written to.
		/// </summary>
		/// <value>The output widget</value>
		public TextView Output {
			get;
			private set;
		}
	}
}

