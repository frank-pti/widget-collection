using Gtk;
using Logging;
using Logging.Backends;
using System;
using System.IO;

namespace Frank.Widgets
{
    /// <summary>
    /// Logger backend that writes the log messages to a TextView widget.
    /// </summary>
    public class LoggerTextViewBackend : Backend
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.LoggerTextViewBackend"/> class.
        /// </summary>
        public LoggerTextViewBackend()
        {
            TextView = new TextView() {
                Editable = false,
                WrapMode = WrapMode.Word
            };
            TextViewStreamWriter = new TextViewStreamWriter(TextView);
        }

        #region implemented abstract members of Backend
        /// <inheritdoc/>
        public override void Log(DateTime timeStamp, LogLevel level, string message)
        {
            TextViewStreamWriter.WriteLine(BuildLogLine(timeStamp, level, message));
        }
        #endregion

        /// <summary>
        /// Gets the text view.
        /// </summary>
        /// <value>The text view.</value>
        public TextView TextView
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the text view stream writer.
        /// </summary>
        /// <value>The text view stream writer.</value>
        public TextWriter TextViewStreamWriter
        {
            get;
            protected set;
        }
    }
}

