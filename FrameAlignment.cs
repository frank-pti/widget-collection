/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
namespace Frank.Widgets
{
    /// <summary>
    /// Alignment that accepts the padding values as constructor parameters with many overloads for simple
    /// initialization of alignments with equal paddings on each side.
    /// </summary>
    public class FrameAlignment: Gtk.Alignment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FrameAlignment"/> class.
        /// </summary>
        /// <param name="padding">equal horizontal and vertical padding.</param>
        public FrameAlignment (uint padding):
            this(padding, padding)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FrameAlignment"/> class.
        /// </summary>
        /// <param name="horizontalPadding">Equal left and right padding.</param>
        /// <param name="verticalPadding">EQual top and bottom padding.</param>
		public FrameAlignment (uint horizontalPadding, uint verticalPadding):
			this(horizontalPadding, horizontalPadding, verticalPadding, verticalPadding)
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FrameAlignment"/> class. Align and scale values
        /// are set to default values.
        /// </summary>
        /// <param name="leftPadding">Left padding.</param>
        /// <param name="rightPadding">Right padding.</param>
        /// <param name="topPadding">Top padding.</param>
        /// <param name="bottomPadding">Bottom padding.</param>
        public FrameAlignment (uint leftPadding, uint rightPadding, uint topPadding, uint bottomPadding):
            this(leftPadding, rightPadding, topPadding, bottomPadding, 0, 0, 1, 1)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FrameAlignment"/> class.
        /// </summary>
        /// <param name="leftPadding">Left padding.</param>
        /// <param name="rightPadding">Right padding.</param>
        /// <param name="topPadding">Top padding.</param>
        /// <param name="bottomPadding">Bottom padding.</param>
        /// <param name="xalign">X align.</param>
        /// <param name="yalign">Y align.</param>
        /// <param name="xscale">X scale.</param>
        /// <param name="yscale">Y scale.</param>
        public FrameAlignment(uint leftPadding, uint rightPadding, uint topPadding, uint bottomPadding, float xalign, float yalign, float xscale, float yscale):
            base(xalign, yalign, xscale, yscale)
        {
            LeftPadding = leftPadding;
            RightPadding = rightPadding;
            TopPadding = topPadding;
            BottomPadding = bottomPadding;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FrameAlignment"/> class.
        /// </summary>
        /// <param name="horizontalPadding">Equal left and right padding.</param>
        /// <param name="verticalPadding">Equal top and bottom padding.</param>
        /// <param name="xalign">Xalign.</param>
        /// <param name="yalign">Yalign.</param>
        /// <param name="xscale">Xscale.</param>
        /// <param name="yscale">Yscale.</param>
        public FrameAlignment (uint horizontalPadding, uint verticalPadding, float xalign, float yalign, float xscale, float yscale):
            this(horizontalPadding, horizontalPadding, verticalPadding, verticalPadding, xalign, yalign, xscale, yscale)
        {
        }
    }
}
