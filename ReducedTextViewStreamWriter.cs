using System;
using System.Text;
using Gtk;

namespace Frank.Widgets
{
    /// <summary>
    /// This class implements a text writer that writes to a text view widget. The output is reduced, that means, if
    /// the same line should be printed several times it is printed just once. A message saying how often it was
    /// repeated is printed when another line occures.
    /// </summary>
	public class ReducedTextViewStreamWriter : TextViewStreamWriter
	{
		private int sameLineCount = 0;
		private string lastLine = "";
		private StringBuilder currentLine = null;

		/// <summary>
		/// Constructs a new instance with the <see cref="TextView"/> to write to.
		/// </summary>
		/// <param name="output">
		/// A <see cref="TextView"/>, where the text should be written to
		/// </param>
		public ReducedTextViewStreamWriter (TextView output):
			base(output)
		{
			currentLine = new StringBuilder ();
		}

		/// <summary>
		/// Writes a character to the specified <see cref="TextView"/> widget.
		/// </summary>
		/// <param name="value">
		/// The character to write to the text stream.
		/// </param>
		public override void Write (char value)
		{
			currentLine.Append (value);
			if (Convert.ToInt32 (10) == Convert.ToInt32 (value)) {
				string tobeadded = currentLine.ToString ();
				if (lastLine != tobeadded) {
					Application.Invoke((o, a) => {
						TextIter start, end;
						Output.Buffer.GetBounds (out start, out end);

						if (sameLineCount > 0) {
							String text = String.Format(">>> Repeated {0} times <<<\r\n", sameLineCount);
							Output.Buffer.Insert(ref end, text);
						}
						Output.Buffer.Insert(ref end, tobeadded);

						TextMark tm = Output.Buffer.GetMark("eot");
						if (tm == null) {
							tm = Output.Buffer.CreateMark("eot", end, false);
						}
						Output.Buffer.MoveMark(tm, end);
						Output.ScrollMarkOnscreen(tm);
					});
					sameLineCount = 0;
				} else {
					sameLineCount++;
				}
				lastLine = tobeadded;
				currentLine = new StringBuilder ();
			}
		}
	}
}

