using System;

namespace Frank.Widgets
{
    /// <summary>
    /// Loggin backend for reduced text output. Uses for a <see cref="ReducedTextViewStreamWriter"/> for writing the
    /// output.
    /// </summary>
	public class LoggerReducedTextViewBackend: LoggerTextViewBackend
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.LoggerReducedTextViewBackend"/> class.
        /// </summary>
		public LoggerReducedTextViewBackend ():
			base()
		{
			TextViewStreamWriter = new ReducedTextViewStreamWriter(TextView);
		}
	}
}

