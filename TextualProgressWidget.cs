/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using InternationalizationUi;
using System.Timers;

namespace Frank.Widgets
{
    /// <summary>
    /// Widget for displaying textual progress.
    /// </summary>
	public class TextualProgressWidget: Gtk.VBox
	{
		private Gtk.Box box;
		private TranslatableLabel label;
		private Gtk.ProgressBar progressBar;
		private Timer timer;
		private bool compact;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.TextualProgressWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="domain">I18n domain.</param>
        /// <param name="message">Message.</param>
        /// <param name="orienation">Orienation.</param>
		public TextualProgressWidget(I18n i18n, string domain, string message, Gtk.Orientation orienation):
			this(i18n.TrObject(domain, message), orienation)
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.TextualProgressWidget"/> class.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <param name="orienation">Orienation.</param>
		public TextualProgressWidget(TranslationString message, Gtk.Orientation orienation) {
			label = new TranslatableLabel(message) {
				Xalign = 0f,
				Visible = false,
				NoShowAll = true
			};
			progressBar = new Gtk.ProgressBar() {
				PulseStep = 0.1,
				Visible = false,
				NoShowAll = true
			};
			switch (orienation) {
			case Gtk.Orientation.Horizontal:
				box = new Gtk.HBox(true, 5);
				break;
			case Gtk.Orientation.Vertical:
				box = new Gtk.VBox(false, 5);
				break;
			default:
				box = new Gtk.VBox(false, 5);
				break;
			}
			box.PackStart(label, true, true, 0);
			box.PackStart(progressBar, false, true, 0);

			PackStart(box, true, true, 0);

			timer = new Timer(100) {
				AutoReset = true
			};
			timer.Elapsed += HandleTimerElapsed;
			label.Translation.Changed += HandleMessageChanged;
		}

		private void HandleMessageChanged (TranslationString translationString)
		{
			Gtk.Application.Invoke(delegate {
				label.Translation = translationString;
				if (compact) {
					progressBar.Text = translationString.Tr;
				} else {
					progressBar.Text = String.Empty;
				}
			});
		}

		private void HandleTimerElapsed (object sender, ElapsedEventArgs e)
		{
			progressBar.Pulse();
		}

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
		public TranslationString Message {
			get {
				return label.Translation;
			}
			set {
				if (label.Translation != null) {
					label.Translation.Changed -= HandleMessageChanged;
				}
				HandleMessageChanged(value);
				label.Translation.Changed += HandleMessageChanged;
			}
		}

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Frank.Widgets.TextualProgressWidget"/> is compact.
        /// </summary>
        /// <value><c>true</c> if compact; otherwise, <c>false</c>.</value>
		public bool Compact {
			get {
				return compact;
			}
			set {
				compact = value;
				label.Visible = !compact;
				HandleMessageChanged(label.Translation);
			}
		}

        /// <summary>
        /// Starts the progress.
        /// </summary>
		public void StartProgress()
		{
			Gtk.Application.Invoke(delegate {
				label.Visible = !compact;
				progressBar.Visible = true;
			});
			progressBar.Activate();
			timer.Start();
		}

        /// <summary>
        /// Stops the progress.
        /// </summary>
		public void StopProgress()
		{
			Gtk.Application.Invoke(delegate {
				label.Visible = false;
				progressBar.Visible = false;
			});
			timer.Stop();
		}
	}
}
