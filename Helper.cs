/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Frank.Widgets
{
    /// <summary>
    /// Collection of helper methods used in this project
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Gets a value indicating whether this <see cref="Frank.Widgets.Helper"/> windows is detected.
        /// </summary>
        /// <value><c>true</c> if running under windows; otherwise, <c>false</c>.</value>
        public static bool WindowsDetected
        {
            get
            {
                PlatformID platform = Environment.OSVersion.Platform;
                return platform == PlatformID.Win32NT || platform == PlatformID.Win32S ||
                    platform == PlatformID.Win32Windows || platform == PlatformID.WinCE;
            }
        }

        /// <summary>
        /// Builds the file filter string for windows file dialogs for a list of filefilters
        /// </summary>
        /// <returns>The file filter string.</returns>
        /// <param name="fileFilters">File filters.</param>
        public static string BuildFileFilterString(IList<FileFilter> fileFilters)
        {
            if (fileFilters == null) {
                return String.Empty;
            }
            string filterString = "";
            foreach (FileFilter filter in fileFilters) {
                if (!String.IsNullOrWhiteSpace(filterString)) {
                    filterString += "|";
                }
                filterString += filter.WindowsFilterString;
            }
            return filterString;
        }

        /// <summary>
        /// Build the resource name from prefix and name
        /// </summary>
        /// <param name="prefix">The resource prefix</param>
        /// <param name="name">The resource name</param>
        /// <returns>The resource name</returns>
        public static string BuildResourceName(string prefix, string name)
        {
            return String.Format("{0}.{1}", prefix, name);
        }

        /// <summary>
        /// Load a GTK image from the resource given by prefix and name
        /// </summary>
        /// <param name="assembly">The assembly containing the image that should be loaded</param>
        /// <param name="prefix">The resource prefix</param>
        /// <param name="name">The resource name</param>
        /// <returns>The image specified by the resource</returns>
        public static Gtk.Image LoadGtkImageFromResource(Assembly assembly, string prefix, string name)
        {
            return new Gtk.Image(assembly, BuildResourceName(prefix, name));
        }
    }
}

