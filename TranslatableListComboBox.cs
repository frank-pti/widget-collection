﻿using Internationalization;
using Logging;
using System;
using System.Collections.Generic;

namespace Frank.Widgets
{
    /// <summary>
    /// ComboBox that displays a list of objects that are translatable, that means that the types have the
    /// <see cref="TranslatableCaptionAttribute"/> defined.
    /// </summary>
    /// <typeparam name="T">The type of the list</typeparam>
    public class TranslatableListComboBox<T> : ListComboBox<T>
        where T : class
    {
        public TranslatableListComboBox(I18n i18n, IList<T> list) :
            base(list, new object[] { i18n })
        {
        }

        protected override string BuildDataString(T item, object[] renderParameters)
        {
            try {
                I18n i18n = (I18n)renderParameters[0];
                object[] attributes = item.GetType().GetCustomAttributes(typeof(TranslatableCaptionAttribute), false);
                string result;
                if (attributes.Length > 0) {
                    result = (attributes[0] as TranslatableCaptionAttribute).GetTranslation(i18n).Tr;
                } else {
                    result = i18n.Tr(Constants.UiDomain, item.ToString());
                }
                return result;
            } catch (Exception e) {
                Logger.Log(e);
                if (e.InnerException != null) {
                    Logger.Log(e.InnerException);
                }
                return base.BuildDataString(item, renderParameters);
            }
        }
    }
}
