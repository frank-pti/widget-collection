﻿using Frank.Widgets.Model;
using Logging;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;

namespace Frank.Widgets
{
    public class PrinterList
    {
        public PrinterList()
        {
            PrintDocument printDoc = new PrintDocument();
            PrinterSettings printerSettings = printDoc.PrinterSettings;
            Printers = new List<PrinterInfo>();
            try {
                Gtk.Printer.EnumeratePrinters(PrinterFunc, true);
                Logger.Log("Printer list created using Printer.EnumeratePrinters");
            } catch (Exception) {
                try {
                    foreach (string printerName in PrinterSettings.InstalledPrinters) {
                        PrinterInfo info = new PrinterInfo() {
                            Name = printerName,
                            Status = String.Empty,
                            Location = String.Empty,
                            Comment = String.Empty,
                            IsDefault = (printerSettings.PrinterName == printerName && printerSettings.IsDefaultPrinter)
                        };
                        Printers.Add(info);
                    }
                    Logger.Log("Printer list created using System.Drawing.Printing.PrinterSettings");
                } catch (Exception) {
                    Logger.Error("Panic: Could not retrieve printer list.");
                }
            }
        }

        private bool PrinterFunc(Gtk.Printer printer)
        {
            Printers.Add(new PrinterInfo() {
                Name = printer.Name,
                Status = printer.StateMessage,
                Location = printer.Location,
                Comment = printer.Description,
                IsDefault = printer.IsDefault
            });
            return false;
        }

        public IList<PrinterInfo> Printers
        {
            get;
            private set;
        }

        public Nullable<PrinterInfo> DefaultPrinter
        {
            get
            {
                IEnumerable<PrinterInfo> printers = Printers.Where(p => p.IsDefault);
                if (printers.Count() < 1) {
                    return null;
                }
                return printers.First();
            }
        }
    }
}
