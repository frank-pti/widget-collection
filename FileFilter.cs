/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Frank.Widgets
{
    /// <summary>
    /// File filter.
    /// </summary>
	public class FileFilter: Gtk.FileFilter
	{
		private IList<string> extensions;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FileFilter"/> class.
        /// </summary>
		public FileFilter ():
			this("")
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FileFilter"/> class.
        /// </summary>
        /// <param name="extension">File extension.</param>
		public FileFilter (string extension):
			base()
		{
            extensions = new List<string>();
			AddExtension(extension);
		}
		
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FileFilter"/> class.
        /// </summary>
        /// <param name="extension">File extension.</param>
        /// <param name="mimeType">MIME type.</param>
		public FileFilter (string extension, string mimeType):
			this(extension)
		{
			AddPattern(FilePattern);
			AddMimeType(mimeType);
		}

        /// <summary>
        /// Adds the extension.
        /// </summary>
        /// <param name="extension">File extension.</param>
        public void AddExtension(string extension)
        {
            extensions.Add(extension);
        }

        /// <summary>
        /// Gets the extension.
        /// </summary>
        /// <value>The extension.</value>
		public string Extension {
			get {
                return GetExtension(0);
			}
		}

        /// <summary>
        /// Gets the extension.
        /// </summary>
        /// <returns>The file extension.</returns>
        /// <param name="index">The index.</param>
        public string GetExtension(int index)
        {
            return extensions[index];
        }

        /// <summary>
        /// Gets the file pattern.
        /// </summary>
        /// <value>The file pattern.</value>
        public string FilePattern {
            get {
                string pattern = "";
                for (int i = 0; i < extensions.Count; i++) {
                    if (i > 0) {
                        pattern += ";";
                    }
                    pattern += GetFilePattern(i);
                }
                return pattern;
            }
        }

        /// <summary>
        /// Gets the file pattern.
        /// </summary>
        /// <returns>The file pattern.</returns>
        /// <param name="index">Index.</param>
        public string GetFilePattern(int index)
        {
            return String.Format("*.{0}", GetExtension(index));
        }

        /// <summary>
        /// Gets the file name suffix.
        /// </summary>
        /// <value>The file name suffix.</value>
		public string FileNameSuffix {
			get {
                if (extensions.Count == 1) {
                    return GetFileNameSuffix(0);
                }
                return String.Empty;
			}
		}

        /// <summary>
        /// Gets the file name suffix (including the '.', e.g. ".txt").
        /// </summary>
        /// <returns>The file name suffix.</returns>
        /// <param name="index">Index.</param>
        public string GetFileNameSuffix(int index)
        {
            return String.Format(".{0}", GetExtension(index));
        }

        /// <summary>
        /// Gets the filter string for use in Windows Forms dialogs in this format: "Name|FilePattern",
        /// e.g. "Text Files|*.txt"
        /// </summary>
        /// <value>The windows filter string.</value>
        public string WindowsFilterString {
            get {
                return String.Format("{0}|{1}", Name, FilePattern);
            }
        }
	}
}

