/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Gtk;
using Internationalization;
using Frank.Widgets.Model;

namespace Frank.Widgets
{
    /// <summary>
    /// ComboBox that reads its drop down list from an enum.
    /// </summary>
	public class TranslatableEnumCombobox<T> : ComboBox
        where T: struct
	{
		private string[] data;
		
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.TranslatableEnumCombobox`1"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="domain">I18n domain.</param>
		public TranslatableEnumCombobox(I18n i18n, string domain) :
			this(i18n, domain, false)
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.TranslatableEnumCombobox`1"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="domain">I18n domain.</param>
        /// <param name="includeExperimental">If set to <c>true</c> include experimental enum values.</param>
		public TranslatableEnumCombobox(I18n i18n, string domain, bool includeExperimental) : base()
		{
			if (!typeof(T).IsEnum) {
				throw new ArgumentException("Generic paramter must be an enum!");
			}
			data = Enum.GetNames(typeof(T));
			
            Model = new TranslatableEnumListStore<T>(i18n, domain, includeExperimental);

			CellRendererText renderer = new CellRendererText();
			PackStart(renderer, false);
            AddAttribute(renderer, "text", 0);
			
			if (data.Length > 0) {
				Active = 0;
			}
		}

        /// <summary>
        /// Gets or sets the active enum.
        /// </summary>
        /// <value>The active enum.</value>
		public T ActiveEnum {
			get {
				if (Active >= 0 && Active < data.Length) {
					return (T) Enum.Parse(typeof(T), data[Active]);
				}
				return default(T);
			}
			set {
				int index = Array.IndexOf<string>(data, value.ToString());
				Active = index;
			}
		}
	}
}

