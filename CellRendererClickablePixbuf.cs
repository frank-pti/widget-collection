﻿using Gdk;
using GLib;
using Gtk;

namespace Frank.Widgets
{
    public class CellRendererClickablePixbuf : CellRendererToggle
    {
        class InternalCellRendererPixbuf : CellRendererPixbuf
        {
            public void StartRender(Drawable window, Widget widget, Rectangle background_area, Rectangle cell_area, Rectangle expose_area, CellRendererState flags)
            {
                base.Render(window, widget, background_area, cell_area, expose_area, flags);
            }
        }

        InternalCellRendererPixbuf cellRendererPixbuf;

        public CellRendererClickablePixbuf() :
            base()
        {
            cellRendererPixbuf = new InternalCellRendererPixbuf();
        }

        [Property("follow-state")]
        public bool FollowState
        {
            get
            {
                return cellRendererPixbuf.FollowState;
            }
            set
            {
                cellRendererPixbuf.FollowState = value;
            }
        }

        [Property("icon-name")]
        public string IconName
        {
            get
            {
                return cellRendererPixbuf.IconName;
            }
            set
            {
                cellRendererPixbuf.IconName = value;
                Activatable = value != null;
            }
        }

        [Property("pixbuf")]
        public Pixbuf Pixbuf
        {
            get
            {
                return cellRendererPixbuf.Pixbuf;
            }
            set
            {
                cellRendererPixbuf.Pixbuf = value;
                Activatable = value != null;
            }
        }

        [Property("pixbuf-expander-closed")]
        public Pixbuf PixbufExpanderClosed
        {
            get
            {
                return cellRendererPixbuf.PixbufExpanderClosed;
            }
            set
            {
                cellRendererPixbuf.PixbufExpanderClosed = value;
            }
        }

        [Property("pixbuf-expander-open")]
        public Pixbuf PixbufExpanderOpen
        {
            get
            {
                return cellRendererPixbuf.PixbufExpanderOpen;
            }
            set
            {
                cellRendererPixbuf.PixbufExpanderOpen = value;
            }
        }

        [Property("stock-detail")]
        public string StockDetail
        {
            get
            {
                return cellRendererPixbuf.StockDetail;
            }
            set
            {
                cellRendererPixbuf.StockDetail = value;
            }
        }

        [Property("stock-id")]
        public string StockId
        {
            get
            {
                return cellRendererPixbuf.StockId;
            }
            set
            {
                cellRendererPixbuf.StockId = value;
                Activatable = value != null;
            }
        }

        [Property("stock-size")]
        public uint StockSize
        {
            get
            {
                return cellRendererPixbuf.StockSize;
            }
            set
            {
                cellRendererPixbuf.StockSize = value;
            }
        }

        public override void GetSize(Widget widget, ref Rectangle cell_area, out int x_offset, out int y_offset, out int width, out int height)
        {
            cellRendererPixbuf.GetSize(widget, ref cell_area, out x_offset, out y_offset, out width, out height);
        }

        protected override void Render(Drawable window, Widget widget, Rectangle background_area, Rectangle cell_area, Rectangle expose_area, CellRendererState flags)
        {
            cellRendererPixbuf.StartRender(window, widget, background_area, cell_area, expose_area, flags);
        }

        public override CellEditable StartEditing(Event evnt, Widget widget, string path, Rectangle background_area, Rectangle cell_area, CellRendererState flags)
        {
            return base.StartEditing(evnt, widget, path, background_area, cell_area, flags);
        }
    }
}
