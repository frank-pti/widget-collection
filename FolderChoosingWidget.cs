/*
 * A collection of Gtk# widgets
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Frank.Widgets.Dialogs;
using Internationalization;
using Logging;
using System;
using System.IO;

namespace Frank.Widgets
{
    /// <summary>
    /// Widget for choosing a folder.
    /// </summary>
    public class FolderChoosingWidget : FileItemChoosingWidget<DirectoryInfo>
    {
        /// <summary>
        /// Occurs when folder changed.
        /// </summary>
        public event EventHandler FolderChanged;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FolderChoosingWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="dialogParent">Dialog parent.</param>
        /// <param name="choosingDialogTitle">Folder dialog title.</param>
        public FolderChoosingWidget(
            I18n i18n, Gtk.Window dialogParent, TranslationString folderDialogTitle) :
            base(i18n, dialogParent, folderDialogTitle)
        {
        }

        protected override void HandleChooseButtonClicked(object sender, EventArgs args)
        {
            try {
                IFolderDialog dialog;
                if (Helper.WindowsDetected) {
                    dialog = new WindowsFolderDialog(choosingDialogTitle);
                } else {
                    dialog = new GtkFolderDialog(i18n, DialogParent, choosingDialogTitle);
                }
                dialog.Folder = Folder;
                DialogResult result = dialog.RunDialog();
                if (result == DialogResult.Ok) {
                    Folder = dialog.Folder;
                }
            } catch (Exception e) {
                Logger.Log(e, "Error while trying to show folder choosing dialog");
            }
        }

        protected override void OnFileItemChanged(EventArgs args)
        {
            if (FolderChanged != null) {
                FolderChanged(this, args);
            }
        }

        /// <summary>
        /// Gets or sets the folder.
        /// </summary>
        /// <value>The folder.</value>
        public DirectoryInfo Folder
        {
            get
            {
                return FileSystemItem;
            }
            set
            {
                FileSystemItem = value;
            }
        }
    }
}

