/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Frank.Widgets.Structs;
using Gtk;
using Internationalization;
using InternationalizationUi;
using Logging.Ui;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Frank.Widgets
{
    /// <summary>
    /// Widget that displays some general program information.
    /// </summary>
    public class HelpWidget : VBox, IContentWidget
    {
        private Table developerInformationTable;
        private Label developerAddressLabel;
        private ScrolledWindow logTextScroll;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.HelpWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="program">Program information.</param>
        /// <param name="developer">Developer information.</param>
        /// <param name="licenses">A list of <see cref="TextFileNotebookPageDetails"/> objects, that should be
        /// displayed. You would probably use this for displaying license information (e.g. GPL)</param>
        public HelpWidget(
            I18n i18n,
            ProgramInformation program,
            DeveloperInformation developer,
            IList<TextFileNotebookPageDetails> licenses) :
            this(i18n, program, developer, licenses, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.HelpWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="program">Program information.</param>
        /// <param name="developer">Developer information.</param>
        /// <param name="licenses">A list of <see cref="TextFileNotebookPageDetails"/> objects, that should be
        /// displayed. You would probably use this for displaying license information (e.g. GPL)</param>
        /// <param name="logTextBuffer">A text buffer where log messages are written to.</param>
        public HelpWidget(
            I18n i18n,
            ProgramInformation program,
            DeveloperInformation developer,
            IList<TextFileNotebookPageDetails> licenses,
            string logDirectoryName)
        {
            Initializer.Initialize(i18n);

            developerInformationTable = new Table(3, 1, false);
            uint y = 0;

            TranslatableMarkupLabel versionLabel = new TranslatableMarkupLabel(
                i18n, Constants.UiDomain, "<b>{0}, Version {1}</b>", program.Name, program.Version);
            versionLabel.Xalign = 0;
            developerInformationTable.Attach(versionLabel, 0, 1, y, y + 1, AttachOptions.Fill, AttachOptions.Shrink, 2, 2);
            y++;

            developerAddressLabel = new Label(String.Format("{0}{1}{2}{1}{3}{1}{4}{1}{5}",
                developer.Name,
                Environment.NewLine,
                developer.Address,
                developer.Place,
                developer.Country,
                developer.Telephone));
            developerAddressLabel.Xalign = 0;
            developerAddressLabel.Selectable = true;
            developerInformationTable.Attach(developerAddressLabel, 0, 1, y, y + 1, AttachOptions.Fill, AttachOptions.Shrink, 2, 2);
            y++;

            Button develperWebsiteButton = new Button(developer.Website);
            develperWebsiteButton.Xalign = 0;
            develperWebsiteButton.Relief = Gtk.ReliefStyle.None;
            develperWebsiteButton.FocusOnClick = false;
            develperWebsiteButton.Clicked += HandleWebsiteButtonClicked;
            developerInformationTable.Attach(develperWebsiteButton, 0, 1, y, y + 1, AttachOptions.Fill, AttachOptions.Shrink, 2, 2);
            y++;

            Button programWebsiteButton = new Button(program.Website);
            programWebsiteButton.Xalign = 0;
            programWebsiteButton.Relief = ReliefStyle.None;
            programWebsiteButton.FocusOnClick = false;
            programWebsiteButton.Clicked += HandleWebsiteButtonClicked;
            developerInformationTable.Attach(programWebsiteButton, 0, 1, y, y + 1, AttachOptions.Fill, AttachOptions.Shrink, 2, 2);
            y++;

            PackStart(developerInformationTable, false, false, 0);

            TextFileNotebook textFileNotebook = new TextFileNotebook(licenses);
            textFileNotebook.SetSizeRequest(600, 200);

            string latestFileName = FindLatestLogFile(logDirectoryName);
            FileStream fileStream = File.Open(latestFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            TextReader reader = new StreamReader(fileStream);
            TextBuffer logTextBuffer = new TextBuffer(new TextTagTable());
            logTextBuffer.Text = reader.ReadToEnd();
            TextView logTextView = new LoggingView(logTextBuffer);
            logTextScroll = new ScrolledWindow();
            logTextScroll.Add(logTextView);
            VBox logBox = new VBox(false, 0);
            logBox.PackStart(logTextScroll, true, true, 0);
            logBox.PackStart(new Entry(latestFileName) { IsEditable = false, Alignment = 0.5f }, false, false, 0);

            textFileNotebook.AppendPage(
                logBox,
                new TranslatableLabel(i18n, Constants.UiDomain, "Program log"));

            PackStart(textFileNotebook, true, true, 5);
        }

        private string FindLatestLogFile(string logDirectoryName)
        {
            DirectoryInfo logDirectory = new DirectoryInfo(logDirectoryName);
            FileInfo[] logFiles = logDirectory.GetFiles("*.log");
            IOrderedEnumerable<FileInfo> ordered = logFiles.OrderBy(file => file.CreationTime);
            return ordered.Last().FullName;
        }

        private void HandleWebsiteButtonClicked(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start((sender as Button).Label);
        }

        public Window DialogParent
        {
            get;
            set;
        }

        public Widget AsWidget
        {
            get
            {
                return this;
            }
        }
    }
}
