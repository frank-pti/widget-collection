using Internationalization;
using InternationalizationUi;
/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Widgets
{
    /// <summary>
    /// Widget for manipulating a data set.
    /// </summary>
    public abstract class SetManipulationWidget : Gtk.VBox
    {
        /// <summary>
        /// Occurs when tree view selection changed.
        /// </summary>
        public event EventHandler TreeViewSelectionChanged;

        private Gtk.TreeView treeView;
        private Gtk.Box buttonBox;
        private TranslatableIconButton addButton;
        private TranslatableIconButton removeButton;
        private TranslatableIconButton copyButton;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.SetManipulationWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="treeView">Tree view.</param>
        /// <param name="expandButtons">If set to <c>true</c> the buttons should expand.</param>
        /// <param name="buttonOrientation">Orientation of the buttons.</param>
        /// <param name="copyButtonVisible">Display copy button.</param>
        public SetManipulationWidget(
            I18n i18n, Gtk.TreeView treeView, bool expandButtons, Gtk.Orientation buttonOrientation, bool copyButtonVisible) :
            base(false, 5)
        {
            this.treeView = treeView;
            this.treeView.Selection.Changed += NotifyTreeviewSelectionChanged;
            this.treeView.Shown += HandleTreeViewShown;
            ExpandButtons = expandButtons;
            Initializer.Initialize(i18n);

            Gtk.ScrolledWindow scroller = new Gtk.ScrolledWindow();
            scroller.Add(treeView);

            addButton = new TranslatableIconButton(i18n, Constants.UiDomain, "_Add", Gtk.Stock.Add) {
                UseUnderline = true
            };
            addButton.Clicked += HandleAddButtonClicked;
            copyButton = new TranslatableIconButton(i18n, Constants.UiDomain, "_Copy", Gtk.Stock.Copy) {
                NoShowAll = true,
                Visible = copyButtonVisible,
                UseUnderline = true
            };
            copyButton.Clicked += HandleCopyButtonClicked;
            removeButton = new TranslatableIconButton(i18n, Constants.UiDomain, "_Remove", Gtk.Stock.Remove) {
                UseUnderline = true
            };
            removeButton.Clicked += HandleRemoveButtonClicked;

            if (buttonOrientation == Gtk.Orientation.Horizontal) {
                buttonBox = new Gtk.HBox(ExpandButtons, 5);
            } else {
                buttonBox = new Gtk.VBox(ExpandButtons, 5);
            }
            buttonBox.PackStart(addButton, ExpandButtons, true, 0);
            buttonBox.PackStart(copyButton, ExpandButtons, true, 0);
            buttonBox.PackStart(removeButton, ExpandButtons, true, 0);
            PackStart(scroller, true, true, 0);
            PackStart(buttonBox, false, true, 0);
        }

        /// <summary>
        /// Handles the remove button clicked.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        protected abstract void HandleRemoveButtonClicked(object sender, EventArgs e);

        /// <summary>
        /// Handles the copy button clicked.
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event args.</param>
        protected abstract void HandleCopyButtonClicked(object sender, EventArgs e);

        /// <summary>
        /// Handles the add button clicked.
        /// </summary>
        /// <param name="sender">Sender object.</param>
        /// <param name="e">Event args.</param>
        protected abstract void HandleAddButtonClicked(object sender, EventArgs e);

        /// <summary>
        /// Updates the button sensitivities.
        /// </summary>
        /// <param name="enableAdd">If set to <c>true</c> enable add.</param>
        /// <param name="enableRemove">If set to <c>true</c> enable remove.</param>
        protected void UpdateButtonSensitivities(bool enableAdd, bool enableCopy, bool enableRemove)
        {
            addButton.Sensitive = enableAdd;
            copyButton.Sensitive = enableCopy;
            removeButton.Sensitive = enableRemove;
        }

        /// <summary>
        /// Adds the additional widget.
        /// </summary>
        /// <param name="button">Button.</param>
        protected void AddAdditionalWidget(Gtk.Widget button)
        {
            buttonBox.PackStart(button, ExpandButtons, true, 0);
        }

        /// <summary>
        /// Gets the tree view.
        /// </summary>
        /// <value>The tree view.</value>
        protected Gtk.TreeView TreeView
        {
            get
            {
                return treeView;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the buttons should expand.
        /// </summary>
        /// <value><c>true</c> if buttons should expand; otherwise, <c>false</c>.</value>
        protected bool ExpandButtons
        {
            get;
            private set;
        }

        private void HandleTreeViewShown(object sender, EventArgs args)
        {
            Gtk.TreeIter iter;
            if (treeView.Model.GetIterFirst(out iter)) {
                treeView.Selection.SelectIter(iter);
            }
        }

        private void NotifyTreeviewSelectionChanged(object sender, EventArgs args)
        {
            if (TreeViewSelectionChanged != null) {
                TreeViewSelectionChanged(sender, args);
            }
        }
    }
}
