﻿using Frank.Widgets.Model;
using Gtk;
using Internationalization;
using System;
using System.IO;

namespace Frank.Widgets
{
    public class UsbDriveComboBox : ComboBox
    {
        private I18n i18n;
        private GenericListStore<DriveInfo> usbDrives;

        public UsbDriveComboBox(I18n i18n)
        {
            this.i18n = i18n;

            CellRendererText textRenderer = new CellRendererText();
            PackStart(textRenderer, false);
            SetCellDataFunc(textRenderer, RenderData);

            usbDrives = new GenericListStore<DriveInfo>();
            foreach (DriveInfo drive in DriveInfo.GetDrives()) {
                if (drive.DriveType == DriveType.Removable) {
                    usbDrives.Add(drive);
                }
            }
            Model = usbDrives;
        }

        private void RenderData(CellLayout cellLayout, CellRenderer cell, TreeModel treeModel, TreeIter iter)
        {
            DriveInfo drive = (DriveInfo)treeModel.GetValue(iter, 0);
            string text = null;
            if (drive.IsReady) {
                string humanReadableSize = BytesToHumanReadable(drive.TotalSize);
                if (String.IsNullOrEmpty(drive.VolumeLabel)) {
                    text = String.Format("{0} - {1}", drive.Name, humanReadableSize);
                } else {
                    text = String.Format("{0} ({1}) - {2}", drive.Name, drive.VolumeLabel, humanReadableSize);
                }
            } else {
                text = String.Format("{0}", drive.RootDirectory);
            }
            (cell as CellRendererText).Text = text;
        }

        public DriveInfo SelectedDrive
        {
            get
            {
                TreeIter iter;
                if (GetActiveIter(out iter)) {
                    return Model.GetValue(iter, 0) as DriveInfo;
                }
                return null;
            }
            set
            {
                if (value == null) {
                    return;
                }
                TreeIter iter;
                Model.GetIterFirst(out iter);
                do {
                    DriveInfo drive = Model.GetValue(iter, 0) as DriveInfo;
                    if (drive != null && drive.Equals(value)) {
                        SetActiveIter(iter);
                    }
                } while (Model.IterNext(ref iter));
            }
        }

        private string BytesToHumanReadable(long bytes)
        {
            string[] unit = new string[] { "B", "KB", "MB", "GB", "TB" };
            int index = 0;
            double result = bytes;
            while (result >= 1024 && index < unit.Length - 1) {
                result /= 1024d;
                index++;
            }
            return String.Format(i18n.NumberFormat, "{0} {1}", Math.Round(result, 2), unit[index]);
        }
    }
}
