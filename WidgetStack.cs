/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Frank.Widgets
{
    public class WidgetStack : Gtk.HBox
    {
        public delegate void ChangedDelegate();
        public event ChangedDelegate Changed;

        private Stack<Gtk.Widget> stack;

        public WidgetStack(Gtk.Widget baseChild)
        {
            stack = new Stack<Gtk.Widget>();
            Push(baseChild);
            ShowAll();
        }

        public void Push(Gtk.Widget child)
        {
            if (stack.Count > 0) {
                Gtk.Widget currentChild = stack.Peek();
                Remove(currentChild);
            }
            stack.Push(child);
            Add(child);
            child.ShowAll();
            NotifyChanged();
        }

        public Gtk.Widget Pop()
        {
            if (stack.Count <= 1) {
                throw new InvalidOperationException("Cannot pop the last stack item");
            }
            Gtk.Widget removed = stack.Pop();
            Remove(removed);
            Add(stack.Peek());
            NotifyChanged();
            return removed;
        }

        public Gtk.Widget PopAndPush(Gtk.Widget child)
        {
            if (stack.Count < 1) {
                throw new InvalidOperationException("Cannot replace the only item on the stack");
            }
            Gtk.Widget removed = stack.Pop();
            Remove(removed);
            Add(child);
            stack.Push(child);
            child.ShowAll();
            NotifyChanged();
            return removed;
        }

        public int Count
        {
            get
            {
                return stack.Count;
            }
        }

        public Gtk.Widget Peek()
        {
            return stack.Peek();
        }

        private void NotifyChanged()
        {
            if (Changed != null) {
                Changed();
            }
        }
    }
}

