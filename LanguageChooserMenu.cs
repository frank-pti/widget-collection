/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using Internationalization.Languages;
using System.Collections.Generic;

namespace Frank.Widgets
{
    /// <summary>
    /// Menu for choosing the language.
    /// </summary>
    public class LanguageChooserMenu: Gtk.Menu
    {
        private Dictionary<Gtk.RadioMenuItem, Language> items;
        private I18n i18n;
        private LanguageConfiguration languageConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.LanguageChooserMenu"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="languageConfiguration">Language configuration.</param>
        public LanguageChooserMenu (I18n i18n, LanguageConfiguration languageConfiguration):
            base()
        {
            this.i18n = i18n;
            this.languageConfiguration = languageConfiguration;
            items = new Dictionary<Gtk.RadioMenuItem, Language>();
            Build();
            this.i18n.LanguageChanged += HandleLanguageChanged;
        }

        private void HandleLanguageChanged (I18n source, string language)
        {
            Gtk.Application.Invoke(delegate {
                Clear();
                Build();
                ShowAll();
            });
        }

        private void HandleActivateItem (object sender, EventArgs e)
        {
            Gtk.RadioMenuItem item = sender as Gtk.RadioMenuItem;
            if (item.Active) {
                i18n.Language = items[sender as Gtk.RadioMenuItem].Id;
            }
        }

        private void Clear()
        {
            foreach (Gtk.RadioMenuItem item in items.Keys) {
                Remove (item);
            }
            items.Clear();
        }

        private void Build ()
        {
            Gtk.RadioMenuItem previousItem = null;
            foreach (Language language in languageConfiguration.Languages) {
                Gtk.RadioMenuItem menuItem = null;
                if (previousItem == null) {
                    menuItem = new Gtk.RadioMenuItem (FormatLanguageName (language, i18n));
                } else {
                    menuItem = new Gtk.RadioMenuItem (previousItem, FormatLanguageName (language, i18n));
                }
                menuItem.Active = (language.Id == i18n.Language);
                menuItem.Toggled += HandleActivateItem;
                Append (menuItem);
                items.Add (menuItem, language);
                previousItem = menuItem;
            }
        }

        private string FormatLanguageName (Language language, I18n i18n)
        {
            return string.Format ("{0} ({1})", language.TranslatedName(i18n), language.Name);
        }
    }
}

