/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Generic list store.
    /// </summary>
    public class GenericListStore<T1, T2> : BaseListStore, IEnumerable<GenericListRow<T1, T2>>, IGenericTreeModel<T1, T2>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Model.GenericListStore`2"/> class.
        /// </summary>
        public GenericListStore() :
            base(typeof(T1), typeof(T2))
        {
        }

        /// <summary>
        /// Add the specified data1 and data2.
        /// </summary>
        /// <param name="data1">Data1.</param>
        /// <param name="data2">Data2.</param>
        public Gtk.TreeIter Add(T1 data1, T2 data2)
        {
            return base.AppendValues(data1, data2);
        }

        /// <summary>
        /// Add the specified data.
        /// </summary>
        /// <param name="data">Data.</param>
        public Gtk.TreeIter Add(Tuple<T1, T2> data)
        {
            return Add(data.Item1, data.Item2);
        }

        /// <summary>
        /// Add the specified dataList.
        /// </summary>
        /// <param name="dataList">Data list.</param>
        public void Add(ICollection<Tuple<T1, T2>> dataList)
        {
            foreach (Tuple<T1, T2> data in dataList) {
                Add(data);
            }
        }

        /// <summary>
        /// Insert the specified data1 and data2 at the specified index.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <param name="data1">Data1.</param>
        /// <param name="data2">Data2.</param>
        public Gtk.TreeIter Insert(int index, T1 data1, T2 data2)
        {
            Gtk.TreeIter iter = base.Insert(index);
            SetValue(iter, 0, data1);
            SetValue(iter, 1, data2);
            return iter;
        }

        /// <summary>
        /// Insert the specified data at the specified index.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <param name="data">Data.</param>
        public Gtk.TreeIter Insert(int index, Tuple<T1, T2> data)
        {
            return Insert(index, data.Item1, data.Item2);
        }

        /// <summary>
        /// Set the specified data at the specified index.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <param name="data">Data.</param>
        public void Set(int index, Tuple<T1, T2> data)
        {
            Set(index, data.Item1, data.Item2);
        }

        /// <summary>
        /// Set the specified data1 and data2 at specified index.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <param name="data1">Data1.</param>
        /// <param name="data2">Data2.</param>
        public void Set(int index, T1 data1, T2 data2)
        {
            Gtk.TreeIter iter;
            base.IterNthChild(out iter, index);
            SetValues(iter, data1, data2);
        }

        /// <summary>
        /// Sets the item1.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <param name="data">Data.</param>
        public void SetItem1(int index, T1 data)
        {
            Gtk.TreeIter iter;
            base.IterNthChild(out iter, index);
            SetValue(iter, 0, data);
        }

        /// <summary>
        /// Sets the item2 at the specified index.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <param name="data">Data.</param>
        public void SetItem2(int index, T2 data)
        {
            Gtk.TreeIter iter;
            base.IterNthChild(out iter, index);
            SetValue(iter, 1, data);
        }

        /// <summary>
        /// Get the item at the specified index.
        /// </summary>
        /// <param name="index">Index.</param>
        public Tuple<T1, T2> Get(int index)
        {
            if (0 > index || index >= Count) {
                throw new IndexOutOfRangeException();
            }
            Gtk.TreeIter iter;
            IterNthChild(out iter, index);
            return new Tuple<T1, T2>((T1)GetValue(iter, 0), (T2)GetValue(iter, 1));
        }

        /// <summary>
        /// Gets the item1.
        /// </summary>
        /// <returns>The item1.</returns>
        /// <param name="index">Index.</param>
        public T1 GetItem1(int index)
        {
            return Get(index).Item1;
        }

        /// <summary>
        /// Gets the item2.
        /// </summary>
        /// <returns>The item2.</returns>
        /// <param name="index">Index.</param>
        public T2 GetItem2(int index)
        {
            return Get(index).Item2;
        }

        /// <summary>
        /// Gets the <see cref="Frank.Widgets.Model.GenericListRow`2"/> at the specified index.
        /// </summary>
        /// <param name="index">Index.</param>
        public GenericListRow<T1, T2> this[int index]
        {
            get
            {
                if (0 > index || index >= Count) {
                    throw new IndexOutOfRangeException();
                }
                return new GenericListRow<T1, T2>(this, index);
            }
        }

        /// <summary>
        /// Gets the <see cref="Frank.Widgets.Model.GenericListRow`2"/> with the specified iter.
        /// </summary>
        /// <param name="iter">Iter.</param>
        public GenericListRow<T1, T2> this[Gtk.TreeIter iter]
        {
            get
            {
                if (!IterIsValid(iter)) {
                    throw new ArgumentException("Invalid iterator", "iter");
                }
                return this[this.GetPath(iter).Indices[0]];
            }
        }

        /// <summary>
        /// Determines whether an element is in the list store
        /// </summary>
        /// <param name="item">the object to locate in the list store.</param>
        /// <returns><c>true</c> if item is found in the list store; otherwise, <c>false</c>.</returns>
        public bool Contains(GenericListRow<T1, T2> item)
        {
            return Contains(item, EqualityComparer<GenericListRow<T1, T2>>.Default);
        }

        /// <summary>
        /// Determines whether an element is in the list store using a specified <see cref="IEqualityComparer<T>"/>.
        /// </summary>
        /// <param name="item">The value to locate in the list store.</param>
        /// <param name="equalityComparer">An equality comparer to compare values.</param>
        /// <returns><c>true</c> if item is found in the list store; otherwise, <c>false</c>.</returns>
        public bool Contains(GenericListRow<T1, T2> item, IEqualityComparer<GenericListRow<T1, T2>> equalityComparer)
        {
            for (int i = 0; i < Count; i++) {
                GenericListRow<T1, T2> t = this[i];
                if (equalityComparer.Equals(t, item)) {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the object.
        /// </summary>
        /// <returns>The object.</returns>
        /// <param name="iter">Iter.</param>
        public override object GetObject(Gtk.TreeIter iter)
        {
            return this[iter];
        }

        #region IEnumerable implementation
        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public new IEnumerator<GenericListRow<T1, T2>> GetEnumerator()
        {
            return new GenericListStoreEnumerator<T1, T2>(this);
        }
        #endregion
    }
}
