/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Generic list store enumerator.
    /// </summary>
	public class GenericListStoreEnumerator<T>:
        BaseListStoreEnumerator<GenericListStore<T>>,
        IEnumerator<T>
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Model.GenericListStoreEnumerator`1"/> class.
        /// </summary>
        /// <param name="listStore">List store.</param>
		public GenericListStoreEnumerator (GenericListStore<T> listStore):
            base(listStore)
		{
		}

		#region IEnumerator implementation
        /// <summary>
        /// Gets the current object the enumerator is pointing at.
        /// </summary>
        /// <value>The current object.</value>
		public new object Current {
			get {
                return listStore[iter];
			}
		}
		#endregion

		#region IEnumerator implementation
        /// <summary>
        /// Gets the current.
        /// </summary>
        /// <value>The current.</value>
		T IEnumerator<T>.Current {
			get {
                return listStore[iter];
			}
		}
		#endregion
	}
}

