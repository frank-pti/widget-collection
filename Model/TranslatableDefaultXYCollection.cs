/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using CairoChart.Model;
using Internationalization;

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Translatable default XY collection for cairo-chart diagrams.
    /// </summary>
    public class TranslatableDefaultXYCollection<TKey, TValue>: DefaultXYCollection<TKey, TValue>
        where TKey: IComparable, IConvertible
        where TValue: IComparable, IConvertible
    {
        private I18n i18n;
        private TranslationString xName;
        private TranslationString xUnit;
        private TranslationString yName;
        private TranslationString yUnit;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Model.TranslatableDefaultXYCollection`2"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="xName">Name of the x axis.</param>
        /// <param name="xUnit">Unit of the x axis values.</param>
        /// <param name="yName">Name of the y axis.</param>
        /// <param name="yUnit">Unit of the y axis values.</param>
        /// <param name="minX">Minimum value of the x values.</param>
        /// <param name="maxX">Maximum value of the x values.</param>
        /// <param name="minY">Minimum value of the y values.</param>
        /// <param name="maxY">Maximum value of the y values.</param>
        public TranslatableDefaultXYCollection (
            I18n i18n,
            TranslationString xName,
            TranslationString xUnit,
            TranslationString yName,
            TranslationString yUnit,
            TKey minX,
            TKey maxX,
            TValue minY,
            TValue maxY):
            base(
                BuildAxisLabel(i18n, xName, xUnit),
                BuildAxisLabel(i18n, yName, yUnit),
                minX, maxX, minY, maxY)
        {
            Initializer.Initialize(i18n);
            this.i18n = i18n;
            this.xName = xName;
            this.xName.Changed += HandleLabelChanged;
            this.xUnit = xUnit;
            this.xUnit.Changed += HandleLabelChanged;
            this.yName = yName;
            this.yName.Changed += HandleLabelChanged;
            this.yUnit = yUnit;
            this.yUnit.Changed += HandleLabelChanged;
        }

        private void HandleLabelChanged (TranslationString translationString)
        {
            XAxis.Label = BuildAxisLabel(i18n, xName, xUnit);
            YAxis.Label = BuildAxisLabel(i18n, yName, yUnit);
        }

        private static string BuildAxisLabel(I18n i18n, TranslationString name, TranslationString unit)
        {
            if (String.IsNullOrWhiteSpace(unit.Tr)) {
                return name.Tr;
            }
            return i18n.Tr(Constants.UiDomain, "{0} [{1}]", name, unit);
        }
    }
}

