/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Base list store enumerator.
    /// </summary>
    public abstract class BaseListStoreEnumerator<T>
        where T: BaseListStore
    {
        /// <summary>
        /// The list store.
        /// </summary>
        protected T listStore;

        /// <summary>
        /// The iter.
        /// </summary>
        protected Gtk.TreeIter iter;

        private bool beforeFirstItem;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Frank.Widgets.Model.BaseListStoreEnumerator`1{T}"/> class.
        /// </summary>
        /// <param name="listStore">List store.</param>
        public BaseListStoreEnumerator(T listStore)
        {
            this.listStore = listStore;
            Reset();
        }

        /// <summary>
        /// Releases all resource used by the <see cref="Frank.Widgets.Model.BaseListStoreEnumerator{T}"/> object.
        /// </summary>
        /// <remarks>Call <see cref="Dispose"/> when you are finished using the
        /// <see cref="Frank.Widgets.Model.BaseListStoreEnumerator`1"/>. The <see cref="Dispose"/> method leaves the
        /// <see cref="Frank.Widgets.Model.BaseListStoreEnumerator`1"/> in an unusable state. After calling
        /// <see cref="Dispose"/>, you must release all references to the
        /// <see cref="Frank.Widgets.Model.BaseListStoreEnumerator`1"/> so the garbage collector can reclaim the memory
        /// that the <see cref="T:Frank.Widgets.Model.BaseListStoreEnumerator`1{T}"/> was occupying.</remarks>
        public void Dispose ()
        {
        }

        /// <summary>
        /// Moves to the next element.
        /// </summary>
        /// <returns><c>true</c>, if it was moved successfully, <c>false</c> otherwise.</returns>
        public bool MoveNext ()
        {
            if (beforeFirstItem) {
                beforeFirstItem = false;
                listStore.GetIterFirst(out iter);
                return listStore.IterIsValid(iter);
            } else {
                return listStore.IterNext(ref iter);
            }
        }

        /// <summary>
        /// Reset this instance.
        /// </summary>
        public void Reset ()
        {
            beforeFirstItem = true;
        }

        /// <summary>
        /// Gets the current object the enumerator is pointing at.
        /// </summary>
        /// <value>The current object.</value>
        public object Current {
            get {
                return listStore.GetObject(iter);
            }
        }
    }
}

