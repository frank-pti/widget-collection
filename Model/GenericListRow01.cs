/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Generic list row
    /// </summary>
    public class GenericListRow<T1>: BaseListRow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Model.GenericListRow`1"/> class.
        /// </summary>
        /// <param name="store">Store.</param>
        /// <param name="rowNum">Row number.</param>
        public GenericListRow(GenericListStore<T1> store, int rowNum):
            base(store, rowNum)
        {
        }

        /// <summary>
        /// Gets the store.
        /// </summary>
        /// <value>The store.</value>
        private GenericListStore<T1> Store {
            get {
                return store as GenericListStore<T1>;
            }
        }

        /// <summary>
        /// Gets or sets the item1.
        /// </summary>
        /// <value>The item1.</value>
        public T1 Item1 {
            get {
                return Store[rowNum];
            }
            set {
                Store[rowNum] = value;
            }
        }
    }
}

