/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Base class for a row of a list store that provides simple data access.
    /// </summary>
    public class BaseListRow
    {
        private bool isValid;

        /// <summary>
        /// The store.
        /// </summary>
        protected BaseListStore store;

        /// <summary>
        /// The row number.
        /// </summary>
        protected int rowNum;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Model.BaseListRow"/> class.
        /// </summary>
        /// <param name="store">The list store.</param>
        /// <param name="rowNum">Row number.</param>
        public BaseListRow(BaseListStore store, int rowNum)
        {
            isValid = (rowNum >= 0 && rowNum < store.Count);
            this.store = store;
            this.rowNum = rowNum;

            store.RowInserted += HandleRowInserted;
            store.RowDeleted += HandleRowDeleted;
        }

        /// <summary>
        /// Handles the row inserted event.
        /// </summary>
        /// <param name="o">The sender object.</param>
        /// <param name="args">The event arguments.</param>
        private void HandleRowInserted(object o, Gtk.RowInsertedArgs args)
        {
            if (args.Path.Indices[0] <= rowNum) {
                rowNum++;
            }
        }

        /// <summary>
        /// Handles the row deleted event.
        /// </summary>
        /// <param name="o">The sender object.</param>
        /// <param name="args">The event arguments.</param>
        private void HandleRowDeleted(object o, Gtk.RowDeletedArgs args)
        {
            if (args.Path.Indices[0] < rowNum) {
                rowNum--;
            } else if (args.Path.Indices[0] == rowNum) {
                isValid = false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        /// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
        public bool IsValid
        {
            get
            {
                return isValid;
            }
        }

        /// <summary>
        /// Gets the iterator.
        /// </summary>
        /// <value>The iterator.</value>
        public Gtk.TreeIter Iterator
        {
            get
            {
                Gtk.TreeIter iter;
                store.IterNthChild(out iter, rowNum);
                return iter;
            }
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (!(obj is BaseListRow)) {
                return false;
            }
            BaseListRow other = (BaseListRow)obj;
            if (other.store.NColumns != this.store.NColumns) {
                return false;
            }
            Gtk.TreeIter thisIter = this.Iterator;
            Gtk.TreeIter otherIter = other.Iterator;
            for (int column = 0; column < store.NColumns; column++) {
                object thisValue = this.store.GetValue(thisIter, column);
                object otherValue = other.store.GetValue(otherIter, column);
                if (!thisValue.Equals(otherValue)) {
                    return false;
                }
            }
            return true;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return base.GetHashCode() ^ store.GetHashCode() ^ rowNum.GetHashCode();
        }
    }
}

