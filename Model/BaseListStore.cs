/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Base class for the generic list stores that provides simple access to its data.
    /// </summary>
    public abstract class BaseListStore : Gtk.ListStore
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Model.BaseListStore"/> class.
        /// </summary>
        /// <param name="types">The column types.</param>
        protected BaseListStore(params Type[] types) :
            base(types)
        {
        }

        /// <summary>
        /// Removes the element at the given index.
        /// </summary>
        /// <param name="index">The index.</param>
        public virtual void RemoveAt(int index)
        {
            Gtk.TreeIter iter;
            IterNthChild(out iter, index);
            if (IterIsValid(iter)) {
                Remove(ref iter);
            }
        }

        /// <summary>
        /// Gets the count of the elements contained n this instance.
        /// </summary>
        /// <value>The amount of elements.</value>
        public int Count
        {
            get
            {
                return IterNChildren();
            }
        }

        /// <summary>
        /// Gets the object.
        /// </summary>
        /// <returns>The object.</returns>
        /// <param name="iter">Iter.</param>
        public abstract object GetObject(Gtk.TreeIter iter);
    }
}

