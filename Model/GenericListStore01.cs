/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Generic list store.
    /// </summary>
    public class GenericListStore<T> : BaseListStore, IEnumerable<T>, IGenericTreeModel<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Model.GenericListStore`1"/> class.
        /// </summary>
        public GenericListStore() :
            base(typeof(T))
        {
        }

        /// <summary>
        /// Add the specified data.
        /// </summary>
        /// <param name="data">Data.</param>
        public virtual Gtk.TreeIter Add(T data)
        {
            return base.AppendValues(data);
        }

        /// <summary>
        /// Add the specified dataList.
        /// </summary>
        /// <param name="dataList">Data list.</param>
        public virtual void Add(ICollection<T> dataList)
        {
            foreach (T data in dataList) {
                Add(data);
            }
        }

        /// <summary>
        /// Removes the first occurrence of data
        /// </summary>
        /// <param name="data">The item to be removed</param>
        /// <returns><c>true</c> if successful removed, <c>false</c> otherwise</returns>
        public virtual bool Remove(T data)
        {
            Gtk.TreeIter nextIter;
            return Remove(data, out nextIter);
        }

        /// <summary>
        /// Removes the first occurrence of data. 
        /// </summary>
        /// <param name="data">The item to be removed</param>
        /// <param name="selectIter">The iter representing the item that can be selected</param>
        /// <returns><c>true</c> if successful removed, <c>false</c> otherwise</returns>
        /// <remarks>The <c>selectIter</c> parameter is set to the next item if available; otherwise to preceding
        /// element or is invalid if no more elements exist.</remarks>
        public virtual bool Remove(T data, out Gtk.TreeIter selectIter)
        {
            Gtk.TreeIter iter;
            base.GetIterFirst(out iter);
            selectIter = iter;
            while (IterIsValid(iter)) {
                T value = (T)base.GetValue(iter, 0);
                if (data.Equals(value)) {
                    bool result = base.Remove(ref iter);
                    if (IterIsValid(iter)) {
                        selectIter = iter;
                    }
                    return result;
                } else {
                    selectIter = iter;
                }
                IterNext(ref iter);
            }
            return false;
        }

        /// <summary>
        /// Insert the specified data at the specified index.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <param name="data">Data.</param>
        public virtual Gtk.TreeIter Insert(int index, T data)
        {
            Gtk.TreeIter iter = base.Insert(index);
            SetValue(iter, 0, data);
            return iter;
        }

        /// <summary>
        /// Gets or sets the data item at the specified index.
        /// </summary>
        /// <param name="index">Index.</param>
        public T this[int index]
        {
            get
            {
                if (0 > index || index >= Count) {
                    throw new IndexOutOfRangeException();
                }
                Gtk.TreeIter iter;
                IterNthChild(out iter, index);
                return this[iter];
            }
            set
            {
                if (0 > index || index >= Count) {
                    throw new IndexOutOfRangeException();
                }
                Gtk.TreeIter iter;
                IterNthChild(out iter, index);
                this[iter] = value;
            }
        }

        /// <summary>
        /// Gets or sets the data item with the specified iter.
        /// </summary>
        /// <param name="iter">Iter.</param>
        public T this[Gtk.TreeIter iter]
        {
            get
            {
                if (!IterIsValid(iter)) {
                    throw new ArgumentException("Invalid iterator", "iter");
                }
                return (T)GetValue(iter, 0);
            }
            set
            {
                if (!IterIsValid(iter)) {
                    throw new ArgumentException("Invalid iterator", "iter");
                }
                SetValue(iter, 0, value);
            }
        }

        /// <summary>
        /// Gets or sets the data item with the specified path.
        /// </summary>
        /// <param name="path">Path.</param>
        public T this[Gtk.TreePath path]
        {
            get
            {
                Gtk.TreeIter iter;
                GetIter(out iter, path);
                if (!IterIsValid(iter)) {
                    throw new ArgumentException("Invalid path", "path");
                }
                return this[iter];
            }
            set
            {
                Gtk.TreeIter iter;
                GetIter(out iter, path);
                if (!IterIsValid(iter)) {
                    throw new ArgumentException("Invalid path", "path");
                }
                this[iter] = value;
            }
        }

        /// <summary>
        /// Determines whether an element is in the list store
        /// </summary>
        /// <param name="item">the object to locate in the list store.</param>
        /// <returns><c>true</c> if item is found in the list store; otherwise, <c>false</c>.</returns>
        public bool Contains(T item)
        {
            return Contains(item, EqualityComparer<T>.Default);
        }

        /// <summary>
        /// Determines whether an element is in the list store using a specified <see cref="IEqualityComparer<T>"/>.
        /// </summary>
        /// <param name="item">The value to locate in the list store.</param>
        /// <param name="equalityComparer">An equality comparer to compare values.</param>
        /// <returns><c>true</c> if item is found in the list store; otherwise, <c>false</c>.</returns>
        public bool Contains(T item, IEqualityComparer<T> equalityComparer)
        {
            for (int i = 0; i < Count; i++) {
                T t = this[i];
                if (equalityComparer.Equals(t, item)) {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the object with the specified iter.
        /// </summary>
        /// <returns>The object.</returns>
        /// <param name="iter">Iter.</param>
        public override object GetObject(Gtk.TreeIter iter)
        {
            return this[iter];
        }

        #region IEnumerable implementation
        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public new IEnumerator<T> GetEnumerator()
        {
            return new GenericListStoreEnumerator<T>(this);
        }
        #endregion
    }
}
