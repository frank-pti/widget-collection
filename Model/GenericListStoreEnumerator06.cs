/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Generic list store enumerator.
    /// </summary>
    public class GenericListStoreEnumerator<T1, T2, T3, T4, T5, T6>:
        BaseListStoreEnumerator<GenericListStore<T1, T2, T3, T4, T5, T6>>,
        IEnumerator<GenericListRow<T1, T2, T3, T4, T5, T6>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Model.GenericListStoreEnumerator`6"/> class.
        /// </summary>
        /// <param name="listStore">List store.</param>
        public GenericListStoreEnumerator (GenericListStore<T1, T2, T3, T4, T5, T6> listStore):
            base(listStore)
        {
            Reset();
        }

        #region IEnumerator implementation
        /// <summary>
        /// Gets the current.
        /// </summary>
        /// <value>The current.</value>
        GenericListRow<T1, T2, T3, T4, T5, T6> IEnumerator<GenericListRow<T1, T2, T3, T4, T5, T6>>.Current {
            get {
                return listStore[iter];
            }
        }
        #endregion
    }
}

