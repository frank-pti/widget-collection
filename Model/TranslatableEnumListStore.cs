/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using System.Reflection;

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Translatable list store for enum values.
    /// </summary>
    public class TranslatableEnumListStore<T>: GenericListStore<string, TranslationString, T>
        where T: struct
    {
        /// <summary>
        /// The index of the name string column.
        /// </summary>
        public const int NameStringColumn = 0;
        /// <summary>
        /// The index of the translation string column.
        /// </summary>
        public const int TranslationStringColumn = 1;
        /// <summary>
        /// The index of the value column.
        /// </summary>
        public const int ValueColumn = 2;

        private string domain;
        private string[] data;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Model.TranslatableEnumListStore`1"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="domain">I18n domain.</param>
        /// <param name="includeExperimental">If set to <c>true</c> include enum values that are marked as experimental
        /// (<see cref="ExperimentalAttribute"/>).</param>
        public TranslatableEnumListStore (I18n i18n, string domain, bool includeExperimental)
        {
            this.domain = domain;
            if (!typeof(T).IsEnum) {
                throw new ArgumentException("Generic paramter must be an enum!");
            }
            data = Enum.GetNames(typeof(T));
            foreach (string item in data) {
                if (!IsExperimental (item) || includeExperimental) {
                    TranslationString translation = GetTranslatableCaption(i18n, domain, item);
                    Add (translation.Tr, translation, EnumByName(item));
                }
            }
            i18n.Changed += HandleI18nChanged;
        }

        private void HandleI18nChanged (I18n source, string domain)
        {
            if (this.domain != domain) {
                return;
            }
            foreach (GenericListRow<string, TranslationString, T> row in this) {
                row.Item1 = row.Item2.Tr;
            }
        }

        private static T EnumByName(string enumName)
        {
            return (T) Enum.Parse(typeof(T), enumName);
        }

        private static TranslationString GetTranslatableCaption(I18n i18n, string domain, string enumName)
        {
            T val = EnumByName(enumName);
            FieldInfo fieldInfo = val.GetType().GetField(enumName);
            TranslatableCaptionAttribute[] attributes = 
                fieldInfo.GetCustomAttributes(typeof(TranslatableCaptionAttribute), false)
                    as TranslatableCaptionAttribute[];
            return (attributes.Length > 0) ? attributes[0].GetTranslation(i18n): 
                new TranslationString(i18n, domain, enumName);
        }

        private static bool IsExperimental(string enumName)
        {
            Enum val = (Enum) Enum.Parse (typeof(T), enumName);
            FieldInfo fieldInfo = val.GetType().GetField (enumName);
            ExperimentalAttribute[] attributes =
                fieldInfo.GetCustomAttributes(typeof(ExperimentalAttribute), false)
                    as ExperimentalAttribute[];
            return attributes != null && attributes.Length > 0;
        }

        /// <summary>
        /// Gets the iter for the given enum value.
        /// </summary>
        /// <returns>The iter.</returns>
        /// <param name="value">Enum value.</param>
        public Gtk.TreeIter GetIter(T value)
        {
            foreach(GenericListRow<string, TranslationString, T> row in this) {
                if (row.Item3.Equals(value))
                {
                    return row.Iterator;
                }
            }
            throw new ArgumentOutOfRangeException();
        }
    }
}

