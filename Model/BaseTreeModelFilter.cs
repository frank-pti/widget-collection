/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Base tree model filter.
    /// </summary>
    public abstract class BaseTreeModelFilter: Gtk.TreeModelFilter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Model.BaseTreeModelFilter"/> class.
        /// </summary>
        /// <param name="childModel">Child model.</param>
        /// <param name="root">Root.</param>
        protected BaseTreeModelFilter (Gtk.TreeModel childModel, Gtk.TreePath root):
            base(childModel, root)
        {
        }
    }
}

