/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Generic list row.
    /// </summary>
    public class GenericListRow<T1, T2, T3, T4, T5, T6, T7, T8, T9>: BaseListRow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Model.GenericListRow`9"/> class.
        /// </summary>
        /// <param name="store">Store.</param>
        /// <param name="rowNum">Row number.</param>
        public GenericListRow(GenericListStore<T1, T2, T3, T4, T5, T6, T7, T8, T9> store, int rowNum):
            base(store, rowNum)
        {
        }

        /// <summary>
        /// Gets the store.
        /// </summary>
        /// <value>The store.</value>
        private GenericListStore<T1, T2, T3, T4, T5, T6, T7, T8, T9> Store {
            get {
                return store as GenericListStore<T1, T2, T3, T4, T5, T6, T7, T8, T9>;
            }
        }

        /// <summary>
        /// Gets or sets the item1.
        /// </summary>
        /// <value>The item1.</value>
        public T1 Item1 {
            get {
                return Store.GetItem1(rowNum);
            }
            set {
                Store.SetItem1(rowNum, value);
            }
        }

        /// <summary>
        /// Gets or sets the item2.
        /// </summary>
        /// <value>The item2.</value>
        public T2 Item2 {
            get {
                return Store.GetItem2 (rowNum);
            }
            set {
                Store.SetItem2 (rowNum, value);
            }
        }

        /// <summary>
        /// Gets or sets the item3.
        /// </summary>
        /// <value>The item3.</value>
        public T3 Item3 {
            get {
                return Store.GetItem3 (rowNum);
            }
            set {
                Store.SetItem3 (rowNum, value);
            }
        }

        /// <summary>
        /// Gets or sets the item4.
        /// </summary>
        /// <value>The item4.</value>
        public T4 Item4 {
            get {
                return Store.GetItem4 (rowNum);
            }
            set {
                Store.SetItem4 (rowNum, value);
            }
        }

        /// <summary>
        /// Gets or sets the item5.
        /// </summary>
        /// <value>The item5.</value>
        public T5 Item5 {
            get {
                return Store.GetItem5 (rowNum);
            }
            set {
                Store.SetItem5 (rowNum, value);
            }
        }

        /// <summary>
        /// Gets or sets the item6.
        /// </summary>
        /// <value>The item6.</value>
        public T6 Item6 {
            get {
                return Store.GetItem6 (rowNum);
            }
            set {
                Store.SetItem6 (rowNum, value);
            }
        }

        /// <summary>
        /// Gets or sets the item7.
        /// </summary>
        /// <value>The item7.</value>
        public T7 Item7 {
            get {
                return Store.GetItem7 (rowNum);
            }
            set {
                Store.SetItem7 (rowNum, value);
            }
        }

        /// <summary>
        /// Gets or sets the item8.
        /// </summary>
        /// <value>The item8.</value>
        public T8 Item8 {
            get {
                return Store.GetItem8 (rowNum);
            }
            set {
                Store.SetItem8 (rowNum, value);
            }
        }

        /// <summary>
        /// Gets or sets the item9.
        /// </summary>
        /// <value>The item9.</value>
        public T9 Item9 {
            get {
                return Store.GetItem9 (rowNum);
            }
            set {
                Store.SetItem9 (rowNum, value);
            }
        }
    }
}

