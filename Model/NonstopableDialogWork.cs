﻿/*
 * A collection of Gtk# widgets
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Frank.Widgets.Dialogs;
using Internationalization;

namespace Frank.Widgets.Model
{
    public abstract class NonstopableDialogWork
    {
        private static NonstopableProgressDialog dialog = null;
        private static int userCouter = 0;

        public NonstopableDialogWork(I18n i18n, TranslationString dialogTitle, TranslationString dialogText, Gtk.Window dialogParent)
        {
            I18n = i18n;
            DialogParent = dialogParent;
            DialogTitle = dialogTitle;
            DialogText = dialogText;
        }

        public NonstopableDialogWork(I18n i18n, TranslationString dialogTitle, TranslationString dialogText, Gdk.Pixbuf dialogIcon)
        {
            I18n = i18n;
            DialogIcon = dialogIcon;
            DialogTitle = dialogTitle;
            DialogText = dialogText;
        }

        protected I18n I18n
        {
            get;
            private set;
        }

        protected Gtk.Window DialogParent
        {
            get;
            private set;
        }

        protected Gdk.Pixbuf DialogIcon
        {
            get;
            private set;
        }

        protected TranslationString DialogTitle
        {
            get;
            private set;
        }

        protected TranslationString DialogText
        {
            get;
            private set;
        }

        public abstract void DoWork();

        protected void StartProgress()
        {
            if (dialog == null) {
                dialog = new NonstopableProgressDialog(
                    I18n, DialogTitle, DialogParent);
                if (DialogIcon != null) {
                    dialog.Icon = DialogIcon;
                }
                dialog.AddMessage(DialogText);
                userCouter++;
                dialog.StartProgress();
            }
            Gtk.Application.Invoke(delegate {
                dialog.Show();
            });
        }

        protected void StopProgress()
        {
            userCouter--;
            if (userCouter < 1) {
                Gtk.Application.Invoke(delegate {
                    dialog.HideAll();
                    dialog.StopProgress();
                    dialog.Destroy();
                    dialog = null;
                });
            }
        }
    }
}
