/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Tree model filter visible func.
    /// </summary>
    public delegate bool TreeModelFilterVisibleFunc<T1, T2, T3, T4>(IGenericTreeModel<T1, T2, T3, T4> model, Gtk.TreeIter iter);

    /// <summary>
    /// Generic tree model filter.
    /// </summary>
    public class GenericTreeModelFilter<T1, T2, T3, T4>: BaseTreeModelFilter, IGenericTreeModel<T1, T2, T3, T4>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Model.GenericTreeModelFilter`4"/> class.
        /// </summary>
        /// <param name="childModel">Child model.</param>
        /// <param name="root">Root.</param>
        public GenericTreeModelFilter(IGenericTreeModel<T1, T2, T3, T4> childModel, Gtk.TreePath root):
            base(childModel, root)
        {
            base.VisibleFunc = new Gtk.TreeModelFilterVisibleFunc(FilterTree);
        }

        private bool FilterTree (Gtk.TreeModel model, Gtk.TreeIter iter)
        {
            if (VisibleFunc == null) {
                return true;
            }
            return VisibleFunc(model as IGenericTreeModel<T1, T2, T3, T4>, iter);
        }

        /// <summary>
        /// Gets or sets the visible func.
        /// </summary>
        /// <value>The visible func.</value>
        public new TreeModelFilterVisibleFunc<T1, T2, T3, T4> VisibleFunc {
            set;
            get;
        }
    }
}