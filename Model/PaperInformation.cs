/*
 * The base library for ProbeNet print exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Frank.Widgets.Model
{
    /// <summary>
    /// Paper information.
    /// </summary>
    public class PaperInformation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Print.PaperInformation"/> class.
        /// </summary>
        /// <param name="width">Width.</param>
        /// <param name="height">Height.</param>
        /// <param name="topMargin">Top margin.</param>
        /// <param name="rightMargin">Right margin.</param>
        /// <param name="bottomMargin">Bottom margin.</param>
        /// <param name="leftMargin">Left margin.</param>
        public PaperInformation(double width, double height, double topMargin, double rightMargin, double bottomMargin, double leftMargin)
        {
            Width = width;
            Height = height;
            TopMargin = topMargin;
            RightMargin = rightMargin;
            BottomMargin = bottomMargin;
            LeftMargin = leftMargin;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.Print.PaperInformation"/> class.
        /// </summary>
        /// <param name="paperSize">Paper size.</param>
        public PaperInformation(Gtk.PaperSize paperSize)
        {
            Width = paperSize.GetWidth(Gtk.Unit.Points);
            Height = paperSize.GetHeight(Gtk.Unit.Points);
            TopMargin = paperSize.GetDefaultTopMargin(Gtk.Unit.Points);
            RightMargin = paperSize.GetDefaultRightMargin(Gtk.Unit.Points);
            BottomMargin = paperSize.GetDefaultBottomMargin(Gtk.Unit.Points);
            LeftMargin = paperSize.GetDefaultLeftMargin(Gtk.Unit.Points);
        }

        private void SwapDirections()
        {
            double originalWidth = Width;
            Width = Height;
            Height = originalWidth;
        }

        /// <summary>
        /// Rotates the left.
        /// </summary>
        public void RotateLeft()
        {
            SwapDirections();
            double originalTopMargin = TopMargin;
            TopMargin = RightMargin;
            RightMargin = BottomMargin;
            BottomMargin = LeftMargin;
            LeftMargin = originalTopMargin;
        }

        /// <summary>
        /// Rotates the right.
        /// </summary>
        public void RotateRight()
        {
            SwapDirections();
            double originalTopMargin = TopMargin;
            TopMargin = LeftMargin;
            LeftMargin = BottomMargin;
            BottomMargin = RightMargin;
            RightMargin = originalTopMargin;
        }

        /// <summary>
        /// Gets or sets the bottom margin.
        /// </summary>
        /// <value>The bottom margin.</value>
        public double BottomMargin
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>The height.</value>
        public double Height
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the left margin.
        /// </summary>
        /// <value>The left margin.</value>
        public double LeftMargin
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the right margin.
        /// </summary>
        /// <value>The right margin.</value>
        public double RightMargin
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the top margin.
        /// </summary>
        /// <value>The top margin.</value>
        public double TopMargin
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>The width.</value>
        public double Width
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the width of the printable area.
        /// </summary>
        /// <value>The width of the printable area.</value>
        public double PrintableWidth
        {
            get
            {
                return Width - LeftMargin - RightMargin;
            }
        }

        /// <summary>
        /// Gets the height of the printable area.
        /// </summary>
        /// <value>The height of the printable area.</value>
        public double PrintableHeight
        {
            get
            {
                return Height - TopMargin - BottomMargin;
            }
        }
    }
}

