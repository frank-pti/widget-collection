﻿/*
 * A collection of Gtk# widgets
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Internationalization;
using InternationalizationUi;
using System;
using System.IO;

namespace Frank.Widgets
{
    public abstract class FileItemChoosingWidget<T> : Gtk.HBox
        where T : FileSystemInfo
    {
        protected I18n i18n;
        protected TranslationString choosingDialogTitle;
        private T fileSystemItem;

        private Gtk.Entry fileItemEntry;
        private TranslatableIconButton chooseButton;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FileItemChoosingWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="dialogParent">Dialog parent.</param>
        /// <param name="choosingDialogTitle">Folder dialog title.</param>
        public FileItemChoosingWidget(
            I18n i18n, Gtk.Window dialogParent, TranslationString choosingDialogTitle) :
            base(false, 5)
        {
            Initializer.Initialize(i18n);
            DialogParent = dialogParent;
            this.i18n = i18n;
            this.choosingDialogTitle = choosingDialogTitle;
            fileItemEntry = new Gtk.Entry() {
                Xalign = 0f,
                Sensitive = true,
                IsEditable = false
            };
            chooseButton = new TranslatableIconButton(
                i18n, Constants.UiDomain, "Ch_oose...", Gtk.Stock.Refresh);
            chooseButton.Clicked += HandleChooseButtonClicked;
            PackStart(fileItemEntry, true, true, 0);
            PackStart(chooseButton, false, true, 0);
        }

        /// <summary>
        /// Handle click of choose button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <remarks>Typical action is displaying an appropriate dialog.</remarks>
        protected abstract void HandleChooseButtonClicked(object sender, EventArgs args);

        /// <summary>
        /// Handle change of file item.
        /// </summary>
        /// <param name="args"></param>
        /// <remarks>Typical actions is raising an event.</remarks>
        protected abstract void OnFileItemChanged(EventArgs args);

        protected T FileSystemItem
        {
            get
            {
                return fileSystemItem;
            }
            set
            {
                T old = fileSystemItem;
                fileSystemItem = value;
                fileItemEntry.Text = fileSystemItem.FullName;
                if (old != fileSystemItem) {
                    OnFileItemChanged(new EventArgs());
                }
            }
        }

        public Gtk.Window DialogParent { get; set; }
    }
}
