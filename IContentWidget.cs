﻿using Gtk;

namespace Frank.Widgets
{
    public interface IContentWidget
    {
        Window DialogParent { get; set; }

        Widget AsWidget { get; }

        void ShowAll();
    }
}
