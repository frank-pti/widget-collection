using Frank.Widgets.Model;
using Internationalization;
using Logging;
using System;
/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.IO;

namespace Frank.Widgets
{
    /// <summary>
    /// ComboBox that displays the file names of a folder in its drop down.
    /// </summary>
    public class FileComboBox : Gtk.ComboBox
    {
        private TranslationString noTemplateString;
        private GenericListStore<FileInfo> fileList;
        private FileSystemWatcher watcher;
        private DirectoryInfo directory;
        private string pattern;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FileComboBox"/> class.
        /// </summary>
        /// <param name="directory">The directory to use.</param>
        /// <param name="pattern">The file name pattern of the files that should be displayed.</param>
        /// <param name="noFileCaption">Translatable caption of the first entry that does not represent a file.
        /// If it is <c>null</c>, no such entry will be added. E.g. for a list of template file names, you may want to
        /// provide an entry "no template".</param>
        public FileComboBox(DirectoryInfo directory, string pattern, TranslationString noFileCaption) :
            base()
        {
            this.directory = directory;
            this.pattern = pattern;
            this.noTemplateString = noFileCaption;

            Gtk.CellRendererText textRenderer = new Gtk.CellRendererText();
            PackStart(textRenderer, false);
            SetCellDataFunc(textRenderer, RenderData);

            fileList = new GenericListStore<FileInfo>();
            if (noFileCaption != null) {
                fileList.Add((FileInfo)null);
            }
            FileInfo[] files = directory.GetFiles(pattern, SearchOption.TopDirectoryOnly);
            Logger.Log("{0}: searching for files in directory '{1}' with pattern '{2}'",
                        GetType().Name, directory.FullName, pattern);
            Logger.Log("Found {0} files in directory '{1}'", files.Length, directory.FullName);
            fileList.Add(files);
            Model = fileList;
            Active = 0;

            watcher = new FileSystemWatcher(directory.FullName, pattern);
            watcher.Created += HandleFileCreated;
            watcher.Renamed += HandleFileRenamed;
            watcher.Deleted += HandleFileDeleted;
            watcher.EnableRaisingEvents = true;
        }

        private void HandleFileCreated(object sender, FileSystemEventArgs e)
        {
            fileList.Add(new FileInfo(e.FullPath));
        }

        private void HandleFileRenamed(object sender, RenamedEventArgs e)
        {
            FileInfo newFile = new FileInfo(e.FullPath);
            int oldFileIndex = FindPositionOfFile(new FileInfo(e.OldFullPath));
            bool oldFileMatched = (oldFileIndex >= 0);
            bool oldFileWasSelected = (Active == oldFileIndex);
            if (oldFileMatched) {
                fileList.RemoveAt(oldFileIndex);
            }
            foreach (FileInfo file in directory.GetFiles(pattern)) {
                if (file.FullName == e.FullPath) {
                    fileList.Add(newFile);
                    if (oldFileWasSelected) {
                        SelectedFileInfo = newFile;
                    }
                    return;
                }
            }
        }

        private int FindPositionOfFile(FileSystemInfo fileToFind)
        {
            for (int i = 0; i < fileList.Count; i++) {
                FileInfo file = fileList[i];
                if (file != null && file.FullName == fileToFind.FullName) {
                    return i;
                }
            }
            return -1;
        }

        private void RemoveFile(FileSystemInfo fileToRemove)
        {
            for (int i = 0; i < fileList.Count; i++) {
                FileInfo file = fileList[i];
                if (file != null && file.FullName == fileToRemove.FullName) {
                    fileList.RemoveAt(i);
                    return;
                }
            }
        }

        private void HandleFileDeleted(object sender, FileSystemEventArgs e)
        {
            RemoveFile(new FileInfo(e.FullPath));
        }

        private void RenderData(Gtk.CellLayout cellLayout, Gtk.CellRenderer cell, Gtk.TreeModel treeModel, Gtk.TreeIter iter)
        {
            FileInfo file = treeModel.GetValue(iter, 0) as FileInfo;
            string text = noTemplateString.Tr;
            if (file != null) {
                text = file.Name;
                if (!ShowExtension) {
                    int pointIndex = text.LastIndexOf('.');
                    if (pointIndex > 0) {
                        text = text.Substring(0, pointIndex);
                    }
                }
            }
            (cell as Gtk.CellRendererText).Text = text;
        }

        /// <summary>
        /// Gets or sets the selected file info.
        /// </summary>
        /// <value>The selected file info.</value>
        public FileInfo SelectedFileInfo
        {
            get
            {
                Gtk.TreeIter iter;
                if (GetActiveIter(out iter)) {
                    return Model.GetValue(iter, 0) as FileInfo;
                }
                return null;
            }
            set
            {
                if (value == null) {
                    Active = 0;
                    return;
                }
                Gtk.TreeIter iter;
                Model.GetIterFirst(out iter);
                do {
                    FileInfo file = Model.GetValue(iter, 0) as FileInfo;
                    if (file != null && String.Equals(file.FullName, value.FullName)) {
                        SetActiveIter(iter);
                    }
                } while (Model.IterNext(ref iter));
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Frank.Widgets.FileComboBox"/> show the file 
        /// extension.
        /// </summary>
        /// <value><c>true</c> if show the file extension; otherwise, <c>false</c>.</value>
        public bool ShowExtension
        {
            get;
            set;
        }
    }
}

