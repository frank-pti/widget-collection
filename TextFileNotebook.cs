/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.IO;
using InternationalizationUi;
using Gtk;
using Frank.Widgets.Structs;

namespace Frank.Widgets
{
    /// <summary>
    /// Notebook that displays several text files.
    /// </summary>
	public class TextFileNotebook: Notebook
	{
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.TextFileNotebook"/> class.
        /// </summary>
        /// <param name="pages">The pages of the notebook.</param>
		public TextFileNotebook (IList<TextFileNotebookPageDetails> pages)
		{
			foreach (TextFileNotebookPageDetails page in pages) {
				AddPage(page);
			}
		}

		private void AddPage(TextFileNotebookPageDetails page)
		{
			TranslatableLabel label = new TranslatableLabel(page.Title);
			TextView textView = new TextView() {
				Editable = false
			};
			Pango.FontDescription font = new Pango.FontDescription() {
				Family = "Monospace",
                AbsoluteSize = 12 * Pango.Scale.PangoScale
			};
			textView.ModifyFont(font);

			StreamReader reader;
			if (page.IsResource && page.Assembly != null) {
				reader = new StreamReader(page.Assembly.GetManifestResourceStream(page.Path));
			} else {
				reader = new StreamReader(page.Path);
			}
			string text = reader.ReadToEnd();
			reader.Close();
			textView.Buffer.Text = text;

			ScrolledWindow scrolledWindow = new ScrolledWindow();
			scrolledWindow.AddWithViewport(textView);			AppendPage(scrolledWindow, label);
		}
	}
}

