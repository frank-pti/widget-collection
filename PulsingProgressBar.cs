/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Timers;

namespace Frank.Widgets
{
    /// <summary>
    /// Pulsing progress bar.
    /// </summary>
    public class PulsingProgressBar: Gtk.ProgressBar
    {
        /// <summary>
        /// The default pulse step.
        /// </summary>
        public const double DefaultPulseStep = 0.1;
        /// <summary>
        /// The default pulse interval.
        /// </summary>
        public const double DefaultPulseInterval = 100;

        private Timer pulseTimer;
        private bool wasVisible;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.PulsingProgressBar"/> class.
        /// </summary>
        public PulsingProgressBar ():
            this(DefaultPulseStep, DefaultPulseInterval)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.PulsingProgressBar"/> class.
        /// </summary>
        /// <param name="pulseStep">Pulse step.</param>
        /// <param name="pulseInterval">Pulse interval.</param>
        public PulsingProgressBar(double pulseStep, double pulseInterval)
        {
            PulseStep = pulseStep;
            pulseTimer = new Timer(pulseInterval) {
                AutoReset = true
            };
            pulseTimer.Elapsed += HandleElapsed;
            wasVisible = Visible;
        }

        private void HandleElapsed (object sender, ElapsedEventArgs e)
        {
            Gtk.Application.Invoke(delegate {
                Pulse();
            });            
        }

        /// <summary>
        /// Start the pulsing progress bar.
        /// </summary>
        public void Start()
        {
            wasVisible = Visible;
            Visible = true;
            Show();
            Activate();
            pulseTimer.Start();
        }

        /// <summary>
        /// Stop the pulsing progress bar.
        /// </summary>
        public void Stop()
        {
            pulseTimer.Stop();
            Visible = wasVisible;
        }
    }
}

