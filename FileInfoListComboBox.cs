﻿using Logging;
using System;
using System.Collections.Generic;
using System.IO;

namespace Frank.Widgets
{
    /// <summary>
    /// ComboBox for choosing from a list of FileInfo objects. The name (without extension) is displayed.
    /// </summary>
    public class FileInfoListComboBox : ListComboBox<FileInfo>
    {
        public FileInfoListComboBox(IList<FileInfo> list, bool displayExtension) :
            base(list, displayExtension)
        {
            DisplayExtension = displayExtension;
        }

        protected override string BuildDataString(FileInfo item, object[] renderParameters)
        {
            try {
                string name = item.Name;
                if (!(bool)renderParameters[0]) {
                    name = name.Substring(0, name.LastIndexOf("."));
                }
                return name;
            } catch (Exception e) {
                Logger.Log(e);
                if (e.InnerException != null) {
                    Logger.Log(e.InnerException);
                }
                return base.BuildDataString(item, renderParameters);
            }
        }

        public string ActiveItemDisplayName
        {
            get
            {
                return BuildDataString(ActiveItem, new object[] { DisplayExtension });
            }
        }

        public bool DisplayExtension
        {
            get;
            private set;
        }
    }
}
