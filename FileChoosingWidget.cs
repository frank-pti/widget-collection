﻿/*
 * A collection of Gtk# widgets
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Frank.Widgets.Dialogs;
using Internationalization;
using Logging;
using System;
using System.IO;

namespace Frank.Widgets
{
    /// <summary>
    /// Widget for choosing a file.
    /// </summary>
    public class FileChoosingWidget : FileItemChoosingWidget<FileInfo>
    {
        /// <summary>
        /// Occurs when file changed.
        /// </summary>
        public event EventHandler FileChanged;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.FileChoosingWidget"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="dialogParent">Dialog parent.</param>
        /// <param name="choosingDialogTitle">Folder dialog title.</param>
        public FileChoosingWidget(
            I18n i18n, Gtk.Window dialogParent, TranslationString fileDialogTitle) :
            base(i18n, dialogParent, fileDialogTitle)
        {
        }

        protected override void HandleChooseButtonClicked(object sender, EventArgs args)
        {
            try {
                IFileDialog dialog;
                if (Helper.WindowsDetected) {
                    dialog = new WindowsFileSaveDialog(choosingDialogTitle);
                } else {
                    dialog = new GtkFileSaveDialog(i18n, DialogParent, null);
                }
                dialog.FileName = File;
                DialogResult result = dialog.RunDialog();
                if (result == DialogResult.Ok) {
                    File = dialog.FileName;
                }
            } catch (Exception e) {
                Logger.Log(e, "Error while trying to show file choosing dialog");
            }
        }

        protected override void OnFileItemChanged(EventArgs args)
        {
            if (FileChanged != null) {
                FileChanged(this, args);
            }
        }

        /// <summary>
        /// Gets or sets the file
        /// </summary>
        /// <value>The file.</value>
        public FileInfo File
        {
            get
            {
                return FileSystemItem;
            }
            set
            {
                FileSystemItem = value;
            }
        }
    }
}
