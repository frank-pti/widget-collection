/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization.Languages;
using Internationalization;

namespace Frank.Widgets
{
    /// <summary>
    /// Combo Box for choosing the UI language.
    /// </summary>
    public class LanguageChooserComboBox: Gtk.ComboBox
    {
        /// <summary>Delegate method for the language chosen event.</summary>
        public delegate void LanguageChosenDelegate(Language language);
        /// <summary>
        /// Occurs when language chosen.
        /// </summary>
        public event LanguageChosenDelegate LanguageChosen;

        private I18n i18n;
        private LanguageConfiguration languageConfiguration;
        private TranslationString lastUsedLanguageString;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.LanguageChooserComboBox"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="languageConfiguration">Language configuration.</param>
        public LanguageChooserComboBox(I18n i18n, LanguageConfiguration languageConfiguration):
            this(i18n, languageConfiguration, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.LanguageChooserComboBox"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="languageConfiguration">Language configuration.</param>
        /// <param name="lastUsedLanguageString">Last used language string.</param>
        public LanguageChooserComboBox(
            I18n i18n, LanguageConfiguration languageConfiguration, TranslationString lastUsedLanguageString)
        {
            this.i18n = i18n;
            this.languageConfiguration = languageConfiguration;
            this.lastUsedLanguageString = lastUsedLanguageString;

            Model = BuildStore();

            Gtk.CellRendererText renderer = new Gtk.CellRendererText();
            PackStart (renderer, true);
            AddAttribute (renderer, "text", 0);

            this.i18n.Changed += HandleLanguageChanged;
            Changed += HandleChanged;
        }

        private void HandleChanged (object sender, EventArgs e)
        {
            Gtk.TreeIter iter;
            GetActiveIter(out iter);
            NotifyLanguageChanged(Model.GetValue (iter, 1) as Language);
        }

        private void HandleLanguageChanged (I18n source, string domain)
        {
            Gtk.TreeIter iter;
            Model.GetIterFirst(out iter);
            do {
                Language language = Model.GetValue (iter, 1) as Language;
                string text = null;
                if (language == null) {
                    text = lastUsedLanguageString.Tr;
                }
                else
                {
                    text = FormatLanguageName(language, source);
                }
                Model.SetValue (iter, 0, text);
            } while (Model.IterNext (ref iter));
        }

        private Gtk.ListStore BuildStore()
        {
            Gtk.ListStore store = new Gtk.ListStore(typeof(string), typeof(Language));
            if (lastUsedLanguageString != null) {
                store.AppendValues(lastUsedLanguageString.Tr, null);
            }
            foreach (Language language in languageConfiguration.Languages) {
                store.AppendValues(FormatLanguageName(language, i18n), language);
            }
            return store;
        }

        private string FormatLanguageName (Language language, I18n i18n)
        {
            return string.Format ("{0} ({1})", language.TranslatedName(i18n), language.Name);
        }

        /// <summary>
        /// Selects the language.
        /// </summary>
        /// <param name="languageId">Language identifier.</param>
        public void SelectLanguage(string languageId)
        {
            Gtk.TreeIter iter;
            Model.GetIterFirst(out iter);
            if (languageId == null && lastUsedLanguageString != null) {
                SetActiveIter(iter);
                return;
            } else {
                do {
                    Language language = (Model.GetValue (iter, 1) as Language);
                    if (language != null && language.Id == languageId) {
                        SetActiveIter(iter);
                        return;
                    }
                } while (Model.IterNext (ref iter));
            }
            throw new ArgumentException(String.Format("No such language: '{0}'", languageId));
        }

        private void NotifyLanguageChanged(Language language)
        {
            if (LanguageChosen != null) {
                LanguageChosen(language);
            }
        }
    }
}

