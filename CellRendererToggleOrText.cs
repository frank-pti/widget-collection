﻿/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gtk;

namespace Frank.Widgets
{
    /// <summary>
    /// Cell renderer that renders a toggle or text. The toggle is rendered if the <see cref="Activate"/> property is set to <c>true</c>,
    /// otherwise the text is rendered.
    /// </summary>
    public class CellRendererToggleOrText : CellRendererToggle
    {
        private static readonly System.Drawing.Color controlLightColor = System.Drawing.SystemColors.ControlLight;
        private static readonly Gdk.Color controlLightGdk = new Gdk.Color(
            controlLightColor.R, controlLightColor.G, controlLightColor.B);

        private class InternalCellRendererText : CellRendererText
        {
            public void StartRender(Gdk.Drawable window, Widget widget, Gdk.Rectangle backgroundArea, Gdk.Rectangle cellArea, Gdk.Rectangle exposeArea, CellRendererState flags)
            {
                Render(window, widget, backgroundArea, cellArea, exposeArea, flags);
            }
        }

        private InternalCellRendererText cellRendererText;

        /// <summary>
        /// Create a new instance of the <see cref="CellRendererToggleOrText"/> class.
        /// </summary>
        public CellRendererToggleOrText()
            : base()
        {
            Initialize();
        }

        private void Initialize()
        {
            cellRendererText = new InternalCellRendererText();
            cellRendererText.BackgroundGdk = controlLightGdk;
        }

        /// <summary>
        /// Gets or sets the text that should be rendered.
        /// </summary>
        [GLib.Property("text")]
        public string Text
        {
            get
            {
                return cellRendererText.Text;
            }
            set
            {
                cellRendererText.Text = value;
            }
        }

        /// <inheritdoc/>
        protected override void Render(Gdk.Drawable window, Widget widget, Gdk.Rectangle backgroundArea, Gdk.Rectangle cellArea, Gdk.Rectangle exposeArea, CellRendererState flags)
        {
            if (Activatable) {
                base.Render(window, widget, backgroundArea, cellArea, exposeArea, flags);
            } else {
                cellRendererText.StartRender(window, widget, backgroundArea, cellArea, exposeArea, flags);
            }
        }

        /// <inheritdoc/>
        public override void GetSize(Widget widget, ref Gdk.Rectangle cellArea, out int xOffset, out int yOffset, out int width, out int height)
        {
            if (Activatable) {
                base.GetSize(widget, ref cellArea, out xOffset, out yOffset, out width, out height);
            } else {
                cellRendererText.GetSize(widget, ref cellArea, out xOffset, out yOffset, out width, out height);
            }
        }
    }
}
