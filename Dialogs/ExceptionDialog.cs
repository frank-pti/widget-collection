/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Widgets.Dialogs
{
    /// <summary>
    /// Display an exception dialog that does not rely on an internationalization instance.
    /// This dialog is a special dialog for errors that occur before the program is fully initialized.
    /// </summary>
    public class ExceptionDialog
    {
        private const int BOX_SPACING = 2;
        private const int BOX_PADDING = 5;

        private Gtk.Clipboard clipboard;
        private Exception exception;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.ExceptionDialog"/> class.
        /// </summary>
        /// <param name="exception">Exception.</param>
        public ExceptionDialog (Exception exception)
        {
            this.exception = exception;

            Gtk.Window window = new Gtk.Window("Exception");
            
            Gtk.VBox box = new Gtk.VBox(false, BOX_SPACING);
            window.Add(box);
            
            Gtk.Label headerLabel = new Gtk.Label() {
                UseMarkup = true,
                Markup = String.Format("Type: <b>{0}</b>", exception.GetType()),
                Selectable = true
            };
            box.PackStart(headerLabel, false, false, BOX_PADDING);
            
            Gtk.Label messageLabel = new Gtk.Label(exception.Message) {
                UseUnderline = false,
                Selectable = true
            };
            box.PackStart(messageLabel, false, false, BOX_PADDING);
            
            Gtk.Expander expander = new Gtk.Expander("Stack Trace");
            box.PackStart(expander, false, false, BOX_PADDING);
            
            Gtk.Label detailLabel = new Gtk.Label(exception.StackTrace) {
                Selectable = true
            };
            expander.Add(detailLabel);

            Gtk.Button copyToClipboardButton = new Gtk.Button("Copy to clipboard");
            copyToClipboardButton.Clicked += HandleCopyToClipboardButtonClicked;
            Gtk.HBox hbox = new Gtk.HBox();
            hbox.PackStart(copyToClipboardButton, false, false, 0);
            box.PackStart(hbox, false, false, BOX_PADDING);

            window.DeleteEvent += HandleWindowDeleteEvent;
            window.ShowAll();

            clipboard = Gtk.Clipboard.Get(Gdk.Atom.Intern("CLIPBOARD", true));
            Gtk.Application.Run();

            throw exception;
        }

        private void HandleWindowDeleteEvent (object o, Gtk.DeleteEventArgs args)
        {
            Gtk.Application.Quit();
        }

        private void HandleCopyToClipboardButtonClicked (object sender, EventArgs e)
        {
            clipboard.Text = String.Format("{0}: {1}{2}{3}",
                exception.GetType(), exception.Message, Environment.NewLine, exception.StackTrace);
        }
    }
}

