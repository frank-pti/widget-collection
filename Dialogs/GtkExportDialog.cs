/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using System.Collections.Generic;

namespace Frank.Widgets.Dialogs
{
	/// <summary>
	/// Export dialog for the GTK environment
	/// </summary>
    public class GtkExportDialog: GtkFileSaveDialog, IFileDialog, IExportDialog
    {
        private I18n i18n;

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.GtkExportDialog"/> class.
		/// </summary>
		/// <param name="i18n">I18n.</param>
		/// <param name="parent">Parent window.</param>
		/// <param name="fileFilters">The file filters.</param>
		/// <param name="exportCurve">If set to <c>true</c> export curve.</param>
		/// <param name="openAfterSave">If set to <c>true</c> open after save.</param>
        public GtkExportDialog (
            I18n i18n, Gtk.Window parent, IList<FileFilter> fileFilters, bool exportCurve, bool openAfterSave):
            base(i18n, parent, fileFilters)
        {
            Initializer.Initialize(i18n);
            this.i18n = i18n;
            ExportCurve = exportCurve;
            OpenAfterSave = openAfterSave;
        }

		/// <inheritdoc/>
        protected override Gtk.FileChooserDialog BuildDialog ()
        {
            Gtk.FileChooserDialog dialog = base.BuildDialog ();
            FileExportSettingsWidget widget = new FileExportSettingsWidget(i18n, CanExportCurve, ExportCurve, OpenAfterSave);
            widget.IncludeCurveDataCheckButton.Toggled +=  delegate(object sender, EventArgs e) {
                ExportCurve = (sender as Gtk.CheckButton).Active;
            };
            widget.OpenAfterSaveCheckButton.Toggled += delegate(object sender, EventArgs e) {
                OpenAfterSave = (sender as Gtk.CheckButton).Active;
            };
            dialog.ExtraWidget = widget;
            dialog.ExtraWidget.ShowAll();
            return dialog;
        }

        #region IExportDialog implementation
        /// <inheritdoc/>
        public bool CanExportCurve {
            get;
            set;
        }

        /// <inheritdoc/>
        public bool ExportCurve {
            get;
            set;
        }

        /// <inheritdoc/>
        public bool OpenAfterSave {
            get;
            set;
        }
        #endregion
    }
}

