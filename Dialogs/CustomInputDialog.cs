﻿using Gdk;
using Gtk;
using Internationalization;
using InternationalizationUi;
using System;
using Window = Gtk.Window;

namespace Frank.Widgets.Dialogs
{
    public class CustomInputDialog : CustomDialog
    {
        private Entry inputEntry;

        public CustomInputDialog(
            I18n i18n,
            Window parentWindow,
            TranslationString title,
            TranslationString inputDescription,
            String initialInputText = "") :
            base(i18n, title, parentWindow, DialogFlags.Modal, ButtonsType.OkCancel)
        {
            TranslatableLabel inputDescriptionLabel = new TranslatableLabel(inputDescription) {
                Xalign = 0f
            };
            inputEntry = new Entry(initialInputText) {
                WidthChars = 21
            };
            VBox.PackStart(inputDescriptionLabel, false, false, 0);
            VBox.PackStart(inputEntry, false, false, 3);
            ShowAll();
        }

        public string Input
        {
            get
            {
                return inputEntry.Text;
            }
        }

        public static string ShowInputDialog(
            I18n i18n,
            Window parentWindow,
            TranslationString title,
            TranslationString inputDescription,
            ResponseHandler responseHandler,
            String initialInputText = "",
            Pixbuf icon = null)
        {
            CustomInputDialog inputDialog = new CustomInputDialog(
                i18n, parentWindow, title, inputDescription, initialInputText);
            CustomMessageDialog.RunDialog(inputDialog, responseHandler, icon);
            return inputDialog.Input;
        }
    }
}
