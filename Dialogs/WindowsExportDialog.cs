/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using Internationalization;

namespace Frank.Widgets.Dialogs
{
	/// <summary>
	/// Export dialog for Windows environments
	/// </summary>
    public class WindowsExportDialog: WindowsFileSaveDialog, IFileDialog, IExportDialog
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.WindowsExportDialog"/> class.
		/// </summary>
		/// <param name="i18n">I18n.</param>
		/// <param name="fileFilters">File filters.</param>
		/// <param name="exportCurve">If set to <c>true</c> export curve.</param>
		/// <param name="openAfterSave">If set to <c>true</c> open after save.</param>
        public WindowsExportDialog (I18n i18n, IList<FileFilter> fileFilters, bool exportCurve, bool openAfterSave):
            base(i18n, fileFilters)
        {
            ExportCurve = exportCurve;
            OpenAfterSave = openAfterSave;
        }

        #region IExportDialog implementation
		/// <inheritdoc/>
        public bool CanExportCurve {
            get;
            set;
        }

		/// <inheritdoc/>
        public bool ExportCurve {
            get;
            set;
        }

		/// <inheritdoc/>
        public bool OpenAfterSave {
            get;
            set;
        }
        #endregion
    }
}

