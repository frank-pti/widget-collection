/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using System.IO;

namespace Frank.Widgets.Dialogs
{
	/// <summary>
	/// Folder dialog for Windows Environmnets.
	/// </summary>
    public class WindowsFolderDialog: IFolderDialog
    {
        private TranslationString description;

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.WindowsFolderDialog"/> class.
		/// </summary>
		/// <param name="description">Description.</param>
        public WindowsFolderDialog (TranslationString description)
        {
            this.description = description;
        }

		/// <summary>
		/// Builds the dialog.
		/// </summary>
		/// <returns>The dialog.</returns>
        protected virtual System.Windows.Forms.FolderBrowserDialog BuildDialog()
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.Description = description.Tr;
            return dialog;
        }

        #region IDialog implementation
		/// <inheritdoc/>
        public DialogResult RunDialog ()
        {
            System.Windows.Forms.FolderBrowserDialog dialog = BuildDialog();
            dialog.SelectedPath = Folder.FullName;
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            Folder = new DirectoryInfo(dialog.SelectedPath);
            switch (result) {
            case System.Windows.Forms.DialogResult.OK:
                return DialogResult.Ok;
            default:
                return DialogResult.Cancel;
            }
        }
        #endregion

        #region IFolderDialog implementation
		/// <inheritdoc/>
        public DirectoryInfo Folder {
            get;
            set;
        }
        #endregion
    }
}

