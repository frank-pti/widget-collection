/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using InternationalizationUi;
using System.Collections.Generic;
using Gtk;

namespace Frank.Widgets.Dialogs
{
	/// <summary>
	/// The custom dialog is a dialog which can display an alert message if the data entered by the user has errors.
	/// Certain responses (e.g. OK) can be disable as long as the information provided by the user is not correct.
	/// </summary>
    public class CustomDialog: TranslatableDialog
    {
        private const string AlertBackgroundColor = "yellow";

        /// <summary>
        /// The i18n.
        /// </summary>
        protected I18n i18n;
        private VBox vBox;
        private HSeparator separator;
        private EventBox alertEventBox;
        private TranslatableIconLabel alertMessage;

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.CustomDialog"/> class.
		/// </summary>
		/// <param name="i18n">I18n.</param>
		/// <param name="title">Title.</param>
		/// <param name="parentWindow">Parent window.</param>
		/// <param name="dialogFlags">Dialog flags.</param>
		/// <param name="buttonsType">Buttons type.</param>
        public CustomDialog(
                I18n i18n,
                TranslationString title,
                Window parentWindow,
				DialogFlags dialogFlags = DialogFlags.Modal,
				ButtonsType buttonsType = ButtonsType.None):
			base(parentWindow, title, dialogFlags)
        {
            this.i18n = i18n;
            Initializer.Initialize(i18n);

            WindowPosition = Gtk.WindowPosition.CenterOnParent;

            vBox = new VBox(false, base.VBox.Spacing);
            base.VBox.PackStart(vBox, true, true, 0);
            separator = new HSeparator() {
                Visible = base.HasSeparator,
                NoShowAll = true
            };
            vBox.PackStart(separator, false, true, 3);

            alertMessage = new TranslatableIconLabel(
                i18n.TrObject (Constants.UiDomain, ""), new Image(Stock.DialogError, IconSize.Button))
            {
                Visible = false,
                NoShowAll = true
            };
            alertEventBox = new EventBox();
            Gdk.Color alertBg = Gdk.Color.Zero;
            Gdk.Color.Parse (AlertBackgroundColor, ref alertBg);
            alertEventBox = new EventBox () {
                Visible = false,
                NoShowAll = true
            };
            alertEventBox.ModifyBg(StateType.Normal, alertBg);
            alertEventBox.Add(alertMessage);
            VBox.PackEnd(alertEventBox, false, true, 3);

			KeyValuePair<Button, ResponseType>[] buttonPairs = BuildAndAddButtons(buttonsType);
			foreach (KeyValuePair<Button, ResponseType> buttonPair in buttonPairs) {
				if (buttonPair.Key.CanDefault) {
					Default = buttonPair.Key;
				}
			}

            (ActionArea as Box).Homogeneous = false;
        }

		private KeyValuePair<Button, ResponseType>[] BuildAndAddButtons(ButtonsType buttonsType)
		{
            KeyValuePair<Button, ResponseType>[] buttonPairs = BuildButtons(i18n, buttonsType);
            foreach (KeyValuePair<Button, ResponseType> pair in buttonPairs) {
                AddButton(pair.Key, pair.Value);
            }
            return buttonPairs;
		}

		/// <summary>
		/// Builds the response button.
		/// </summary>
		/// <returns>The response button.</returns>
		/// <param name="text">Translatable text for the button.</param>
		/// <param name="stock">Stock icon name.</param>
		/// <param name="response">Response type of the button.</param>
		/// <param name="canDefault">If set to <c>true</c> the button can be the default.</param>
        public static KeyValuePair<Button, ResponseType> BuildResponseButton(TranslationString text, string stock, ResponseType response, bool canDefault = false)
        {
            Image icon = new Image(stock, IconSize.Button);
            TranslatableIconButton button = new TranslatableIconButton(text, icon) {
				CanDefault = canDefault,
				UseUnderline = true,
			};
            return new KeyValuePair<Button, ResponseType>(button, response);
        }

        private static KeyValuePair<Button, ResponseType>[] BuildButtons (I18n i18n, ButtonsType buttons)
        {
            switch (buttons) {
            case ButtonsType.Ok:
                return new KeyValuePair<Button, ResponseType>[] {
                     BuildResponseButton (i18n.TrObject (Constants.UiDomain, "_Ok"), Stock.Ok, ResponseType.Ok, true)
                 };
            case ButtonsType.OkCancel:
                return new KeyValuePair<Button, ResponseType>[] {
                     BuildResponseButton (i18n.TrObject (Constants.UiDomain, "_Ok"), Stock.Ok, ResponseType.Ok, true),
                     BuildResponseButton (i18n.TrObject (Constants.UiDomain, "_Cancel"), Stock.Cancel, ResponseType.Cancel)
                 };
            case ButtonsType.YesNo:
                return new KeyValuePair<Button, ResponseType>[] {
                     BuildResponseButton (i18n.TrObject (Constants.UiDomain, "_Yes"), Stock.Yes, ResponseType.Yes),
                     BuildResponseButton (i18n.TrObject (Constants.UiDomain, "_No"), Stock.No, ResponseType.No, true)
                 };
            case ButtonsType.Cancel:
                return new KeyValuePair<Button, ResponseType>[] {
                     BuildResponseButton (i18n.TrObject (Constants.UiDomain, "_Cancel"), Stock.Cancel, ResponseType.Cancel, true)
                 };
            case ButtonsType.Close:
                return new KeyValuePair<Button, ResponseType>[] {
                     BuildResponseButton (i18n.TrObject (Constants.UiDomain, "_Close"), Stock.Close, ResponseType.Close, true)
                 };
            default:
                return new KeyValuePair<Button, ResponseType>[0];
            }
        }

		/// <summary>
		/// Gets or sets a value indicating whether the dialog has a separator.
		/// </summary>
		/// <value><c>true</c> if this dialog has separator; otherwise, <c>false</c>.</value>
        public new bool HasSeparator {
            get {
                return base.HasSeparator;
            }
            set {
                base.HasSeparator = value;
                separator.Visible = value;
            }
        }

		/// <summary>
		/// Gets the container widget for the dialog controls. Pack your dialog content here.
		/// </summary>
		/// <value>The container widget.</value>
        public new VBox VBox {
            get {
                return vBox;
            }
        }

		/// <summary>
		/// Add an additional button to the button area.
		/// </summary>
		/// <returns>The button.</returns>
		/// <param name="buttonText">Button text.</param>
		/// <param name="response">Response.</param>
        protected new Widget AddButton(string buttonText, ResponseType response)
        {
            return AddButton (new Button(buttonText), response);
        }

		/// <summary>
		/// Add an additional button to the button area.
		/// </summary>
		/// <returns>The button.</returns>
		/// <param name="buttonText">Button text.</param>
		/// <param name="response">Response.</param>
        protected new Widget AddButton(string buttonText, int response)
        {
            return AddButton (buttonText, (ResponseType)response);
        }

		/// <summary>
		/// Add an additional button to the button area.
		/// </summary>
		/// <returns>The button.</returns>
		/// <param name="button">Button.</param>
		/// <param name="response">Response.</param>
        protected Button AddButton(Button button, ResponseType response)
        {
            AddActionWidget(button, response);
            return button;
        }

		/// <summary>
		/// Displaies the alert.
		/// </summary>
		/// <param name="message">Translatable message.</param>
		/// <param name="disableResponse">The type of repsonse that should be disabled (e.g. Ok).
		/// Pass <code>null</code> if no response type should be disabled.</param>
        protected void DisplayAlert(TranslationString message, Nullable<ResponseType> disableResponse)
        {
            Application.Invoke(delegate {
                Image image = new Image(Stock.DialogError, IconSize.Button);
                alertMessage.Translation = message;
                alertMessage.Icon = image;
                SetAlertVisibility(true, disableResponse, false);
                ShowAll();
            });
        }

		/// <summary>
		/// Hides the alert.
		/// </summary>
		/// <param name="enableResponse">The type of response that should be enabled (e.g. Ok).
		/// Pass <code>null</code> if no response type should be enabled.</param>
        protected void HideAlert(Nullable<ResponseType> enableResponse)
        {
            Application.Invoke (delegate {
                SetAlertVisibility(false, enableResponse, true);
            });
        }

        private void SetAlertVisibility(bool visible, Nullable<ResponseType> responseSensitivity, bool enableResponse)
        {
            alertMessage.NoShowAll = !visible;
            alertMessage.Visible = visible;
            if (responseSensitivity != null) {
                SetResponseSensitive(responseSensitivity.Value, enableResponse);
            }
            alertEventBox.NoShowAll = !visible;
            alertEventBox.Visible = visible;
        }
    }
}

