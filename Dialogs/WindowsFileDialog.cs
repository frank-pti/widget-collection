/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.IO;

namespace Frank.Widgets.Dialogs
{
	/// <summary>
	/// File dialog for Windows environments.
	/// </summary>
    public abstract class WindowsFileDialog: IFileDialog
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.WindowsFileDialog"/> class.
		/// </summary>
        public WindowsFileDialog ()
        {
        }

		/// <summary>
		/// Builds the dialog.
		/// </summary>
		/// <returns>The dialog.</returns>
        protected abstract System.Windows.Forms.FileDialog BuildDialog();

        #region IDialog implementation
		/// <inheritdoc/>
        public DialogResult RunDialog ()
        {
            System.Windows.Forms.FileDialog dialog = BuildDialog ();
            int originalFilterIndex = 0;
            if (FileFilters != null) {
                originalFilterIndex = FileFilters.IndexOf (SelectedFileFilter) + 1;
                if (originalFilterIndex == 0) {
                    originalFilterIndex = 1;
                }
                dialog.FilterIndex = originalFilterIndex;
            }
            FileInfo originalFile = FileName;
            if (FileName != null) {
                dialog.InitialDirectory = FileName.DirectoryName;
                dialog.FileName = FileName.FullName;
            }
            
            System.Windows.Forms.DialogResult result = dialog.ShowDialog ();
            
            if (String.IsNullOrWhiteSpace (dialog.FileName)) {
                FileName = originalFile;
            } else {
                FileName = new FileInfo (dialog.FileName);
            }
            if (FileFilters != null) {
                if (dialog.FilterIndex > 0 && dialog.FilterIndex <= FileFilters.Count) {
                    SelectedFileFilter = FileFilters [dialog.FilterIndex - 1];
                } else {
                    SelectedFileFilter = FileFilters [originalFilterIndex - 1];
                }
            }
            
            switch (result) {
            case System.Windows.Forms.DialogResult.OK:
                return DialogResult.Ok;
            default:
                return DialogResult.Cancel;
            }
        }
        #endregion

        #region IFileDialog implementation
		/// <inheritdoc/>
        public abstract System.IO.FileInfo FileName {
            get;
            set;
        }

		/// <inheritdoc/>
        public abstract IList<FileFilter> FileFilters {
            get;
            set;
        }

		/// <inheritdoc/>
        public abstract FileFilter SelectedFileFilter {
            get;
            set;
        }
        #endregion
    }
}

