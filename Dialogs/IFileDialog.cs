/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.IO;

namespace Frank.Widgets.Dialogs
{
	/// <summary>
	/// Interface for file dialogs
	/// </summary>
    public interface IFileDialog: IDialog
    {
		/// <summary>
		/// Gets or sets the name of the file.
		/// </summary>
		/// <value>The name of the file.</value>
        FileInfo FileName {
            get;
            set;
        }

		/// <summary>
		/// Gets or sets the file filters.
		/// </summary>
		/// <value>The file filters.</value>
        IList<FileFilter> FileFilters {
            get;
            set;
        }

		/// <summary>
		/// Gets or sets the selected file filter.
		/// </summary>
		/// <value>The selected file filter.</value>
        FileFilter SelectedFileFilter {
            get;
            set;
        }
    }
}

