/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using Frank.Widgets.Dialogs;
using InternationalizationUi;
using Logging;

namespace Frank.Widgets.Dialogs
{
	/// <summary>
	/// Progress dialog that displays some text
	/// </summary>
    public abstract class TextualProgressDialog: CustomDialog
    {
        private TranslatableButton closeButton;
		private PulsingProgressBar progressBar;
        private Gtk.VBox messageBox;

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.TextualProgressDialog"/> class.
		/// </summary>
		/// <param name="i18n">I18n.</param>
		/// <param name="title">Title.</param>
		/// <param name="dialogParent">Dialog parent.</param>
        public TextualProgressDialog(I18n i18n, TranslationString title, Gtk.Window dialogParent):
            base(i18n, title, dialogParent, Gtk.DialogFlags.Modal, Gtk.ButtonsType.Close)
        {
            messageBox = new Gtk.VBox();
            VBox.PackStart(messageBox, true, false, 0);

			progressBar = new PulsingProgressBar() {
				Visible = false,
				NoShowAll = true
			};
			VBox.PackStart(progressBar, false, true, 5);

            closeButton = new TranslatableButton(i18n, Constants.UiDomain, "_Stop") {
				UseUnderline = true
			};
            closeButton.Clicked += HandleStopClicked;
        }

		/// <summary>
		/// Handles the stop clicked.
		/// </summary>
		/// <param name="sender">Sender object.</param>
		/// <param name="e">Event arguments</param>
        protected abstract void HandleStopClicked (object sender, EventArgs e);

		/// <summary>
		/// Adds the message.
		/// </summary>
		/// <param name="message">Translatable message.</param>
        public void AddMessage(TranslationString message)
        {
            TranslatableLabel label = new TranslatableLabel(message) {
                Xalign = 0f,
                Wrap = true,
                LineWrap = true,
                UseUnderline = true
            };
            messageBox.PackStart(label, false, false, 5);
            messageBox.ShowAll ();
            label.Show();
        }
        
		/// <summary>
		/// Shows the message.
		/// </summary>
		/// <param name="message">Translatable message.</param>
        public void ShowMessage(TranslationString message)
        {
            Logger.Log(message.UntranslatedString);
            Gtk.Application.Invoke(delegate {
                AddMessage (message);
            });
        }

		/// <summary>
		/// Gets the close button.
		/// </summary>
		/// <value>The close button.</value>
        protected TranslatableButton CloseButton {
            get {
                return closeButton;
            }
        }

		/// <summary>
		/// Starts the progress.
		/// </summary>
		public void StartProgress()
		{
			Gtk.Application.Invoke(delegate {
                progressBar.Start();
			});
		}

		/// <summary>
		/// Stops the progress.
		/// </summary>
		public void StopProgress()
		{
			Gtk.Application.Invoke(delegate {
                progressBar.Stop();
			});
		}
    }
}
