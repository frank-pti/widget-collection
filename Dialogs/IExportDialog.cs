/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Widgets.Dialogs
{
	/// <summary>
	/// Interface for file dialogs for exporting data.
	/// </summary>
    public interface IExportDialog: IFileDialog
    {
		/// <summary>
		/// Gets or sets a value indicating whether the curve can be exported.
		/// </summary>
		/// <value><c>true</c> if the curve can be exported; otherwise, <c>false</c>.</value>
        bool CanExportCurve {
            get;
            set;
        }

		/// <summary>
		/// Gets or sets a value indicating whether the curve should be exported.
		/// </summary>
		/// <value><c>true</c> if the curve should be exported; otherwise, <c>false</c>.</value>
        bool ExportCurve {
            get;
            set;
        }

		/// <summary>
		/// Gets or sets a value indicating whether the exported file should be opened when saving is finished.
		/// </summary>
		/// <value><c>true</c> if the file should be opened after save; otherwise, <c>false</c>.</value>
        bool OpenAfterSave {
            get;
            set;
        }
    }
}

