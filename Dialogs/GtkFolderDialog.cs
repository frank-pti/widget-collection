/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using InternationalizationUi;
using System.IO;

namespace Frank.Widgets.Dialogs
{
	/// <summary>
	/// Gtk dialog for choosing a folder.
	/// </summary>
    public class GtkFolderDialog: IFolderDialog
    {
        private I18n i18n;
        private Gtk.Window parent;
        private TranslationString title;
        private DialogResult dialogResult;

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.GtkFolderDialog"/> class.
		/// </summary>
		/// <param name="i18n">I18n.</param>
		/// <param name="parent">Parent.</param>
		/// <param name="title">Title.</param>
        public GtkFolderDialog (I18n i18n, Gtk.Window parent, TranslationString title)
        {
            Initializer.Initialize(i18n);
            this.i18n = i18n;
            this.parent = parent;
            this.title = title;
        }

		/// <inheritdoc/>
        protected virtual Gtk.FileChooserDialog BuildDialog()
        {
            Gtk.FileChooserDialog dialog = new Gtk.FileChooserDialog(
                title.Tr, parent, Gtk.FileChooserAction.SelectFolder);
            dialog.AddActionWidget(
                new TranslatableIconButton(i18n, Constants.UiDomain, "Ch_oose", Gtk.Stock.Apply) {
                    CanDefault = true
                }, Gtk.ResponseType.Ok);
            dialog.AddActionWidget(
                new TranslatableIconButton(i18n, Constants.UiDomain, "_Cancel", Gtk.Stock.Cancel),
                Gtk.ResponseType.Cancel);
            return dialog;
        }

        #region IDialog implementation
		/// <inheritdoc/>
        public DialogResult RunDialog ()
        {
            Gtk.FileChooserDialog dialog = BuildDialog();
            dialog.SetCurrentFolder(Folder.FullName);
            dialog.SelectFilename(Folder.Name);
            dialog.Response += HandleResponse;;
            dialog.ShowAll();
            dialog.Run();
            Folder = new DirectoryInfo(dialog.Filename);
            dialog.Destroy();
            return dialogResult;
        }
        #endregion

		private void HandleResponse (object o, Gtk.ResponseArgs args)
		{
			switch (args.ResponseId) {
			case Gtk.ResponseType.Accept:
			case Gtk.ResponseType.Ok:
			case Gtk.ResponseType.Apply:
				dialogResult = DialogResult.Ok;
				break;
			default:
				dialogResult = DialogResult.Cancel;
				break;
			}
		}

        #region IFolderDialog implementation
		/// <inheritdoc/>
        public DirectoryInfo Folder {
            get;
            set;
        }
        #endregion

    }
}

