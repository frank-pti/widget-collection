/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using InternationalizationUi;

namespace Frank.Widgets.Dialogs
{
	/// <summary>
	/// File open dialog for Gtk environment
	/// </summary>
    public class GtkFileOpenDialog: GtkFileDialog
    {
        private I18n i18n;
        private Gtk.Window parent;
        private TranslationString title;
        private EventHandler updatePreviewHandler;

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.GtkFileOpenDialog"/> class.
		/// </summary>
		/// <param name="i18n">I18n.</param>
		/// <param name="parent">Parent window.</param>
		/// <param name="title">Translatable title.</param>
		/// <param name="updatePreviewHandler">Handler delegat for updating preview.</param>
        public GtkFileOpenDialog (
            I18n i18n, Gtk.Window parent, TranslationString title, EventHandler updatePreviewHandler)
        {
            Initializer.Initialize(i18n);
            this.i18n = i18n;
            this.parent = parent;
            this.title = title;
            this.updatePreviewHandler = updatePreviewHandler;
        }

		/// <inheritdoc/>
        protected override Gtk.FileChooserDialog BuildDialog()
        {
            Gtk.FileChooserDialog dialog = new Gtk.FileChooserDialog(title.Tr, parent, Gtk.FileChooserAction.Open);
            dialog.AddActionWidget(
                new TranslatableIconButton(i18n, Constants.UiDomain, "_Open", Gtk.Stock.Save) {
                    CanDefault = true
                }, Gtk.ResponseType.Ok);
            dialog.AddActionWidget(
                new TranslatableIconButton(i18n, Constants.UiDomain, "_Cancel", Gtk.Stock.Cancel),
                Gtk.ResponseType.Cancel);
            if (FileFilters != null) {
                foreach (FileFilter filter in FileFilters) {
                    dialog.AddFilter (filter);
                }
                if (SelectedFileFilter == null) {
                    SelectedFileFilter = FileFilters[0];
                }
                dialog.Filter = SelectedFileFilter;
            }
            if (updatePreviewHandler != null) {
                dialog.PreviewWidgetActive = true;
                dialog.UpdatePreview += updatePreviewHandler;
            }
            return dialog;
        }
    }
}

