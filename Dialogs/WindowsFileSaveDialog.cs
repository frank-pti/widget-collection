/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using Internationalization;
using System.IO;

namespace Frank.Widgets.Dialogs
{
	/// <summary>
	/// File save dialog for Windows environments.
	/// </summary>
    public class WindowsFileSaveDialog: WindowsFileDialog
    {
        private TranslationString title;

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.WindowsFileSaveDialog"/> class.
		/// </summary>
		/// <param name="title">Title.</param>
        public WindowsFileSaveDialog (TranslationString title):
            this(title, null)
        {
        }

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.WindowsFileSaveDialog"/> class.
		/// </summary>
		/// <param name="i18n">I18n.</param>
        public WindowsFileSaveDialog (I18n i18n):
            this(i18n, null)
        {
        }

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.WindowsFileSaveDialog"/> class.
		/// </summary>
		/// <param name="i18n">I18n.</param>
		/// <param name="fileFilters">File filters.</param>
        public WindowsFileSaveDialog (I18n i18n, IList<FileFilter> fileFilters):
            this(BuildDefaultTitle(i18n), fileFilters)
        {
        }

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.WindowsFileSaveDialog"/> class.
		/// </summary>
		/// <param name="title">Title.</param>
		/// <param name="fileFilters">File filters.</param>
        public WindowsFileSaveDialog (TranslationString title, IList<FileFilter> fileFilters)
        {
            this.title = title;
            FileFilters = fileFilters;
        }

        private static TranslationString BuildDefaultTitle(I18n i18n)
        {
            Initializer.Initialize(i18n);
            return i18n.TrObject(Constants.UiDomain, "Save file");
        }

		/// <summary>
		/// Builds the dialog.
		/// </summary>
		/// <returns>The dialog.</returns>
        protected override System.Windows.Forms.FileDialog BuildDialog()
        {
            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
            dialog.SupportMultiDottedExtensions = true;
            dialog.Title = title.Tr;
            dialog.Filter = Helper.BuildFileFilterString(FileFilters);
            return dialog;
        }

        #region IFileDialog implementation
		/// <inheritdoc/>
        public override FileInfo FileName {
            get;
            set;
        }

		/// <inheritdoc/>
        public override IList<FileFilter> FileFilters {
            get;
            set;
        }

		/// <inheritdoc/>
        public override FileFilter SelectedFileFilter {
            get;
            set;
        }
        #endregion
    }
}

