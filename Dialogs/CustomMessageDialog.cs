/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gdk;
using Gtk;
using Internationalization;
using InternationalizationUi;
using System;
using Image = Gtk.Image;
using Window = Gtk.Window;

namespace Frank.Widgets.Dialogs
{
    /// <summary>
    /// Custom message dialog for displaying common dialogs for information, questions, warnings and errors.
    /// </summary>
    public class CustomMessageDialog : CustomDialog
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.CustomMessageDialog"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="parent">Parent window.</param>
        /// <param name="title">Translatable dialog title.</param>
        /// <param name="message">Translatable message.</param>
        /// <param name="type">Message type.</param>
        /// <param name="buttons">Standard response buttons like Ok, Cancel, Yes, No.</param>
        /// <param name="additonalButtons">Additonal buttons.</param>
        public CustomMessageDialog(
            I18n i18n,
            Window parent,
            TranslationString title,
            TranslationString message,
            MessageType type,
            ButtonsType buttons,
            params Tuple<Button, ResponseType>[] additonalButtons) :
            base(i18n, title, parent, DialogFlags.Modal, buttons)
        {
            base.Resizable = false;

            TranslatableIconLabel iconLabel = new TranslatableIconLabel(message, LoadImage(type)) {
                Wrap = true,
                LineWrapMode = Pango.WrapMode.WordChar
            };
            Alignment alignment = new FrameAlignment(5);
            alignment.Child = iconLabel;
            VBox.PackStart(alignment);

            if (additonalButtons != null) {
                foreach (Tuple<Button, ResponseType> tuple in additonalButtons) {
                    AddButton(tuple.Item1, tuple.Item2);
                }
            }

            ShowAll();
        }

        private static Image LoadImage(MessageType type)
        {
            string stock = null;
            switch (type) {
                case MessageType.Error:
                    stock = Stock.DialogError;
                    break;
                case MessageType.Info:
                    stock = Stock.DialogInfo;
                    break;
                case MessageType.Other:
                    stock = Stock.DialogInfo;
                    break;
                case MessageType.Question:
                    stock = Stock.DialogQuestion;
                    break;
                case MessageType.Warning:
                    stock = Stock.DialogWarning;
                    break;
                default:
                    break;
            }
            if (stock != null) {
                return new Image(stock, IconSize.Dialog);
            }
            return null;
        }

        /// <summary>
        /// Shows an error dialog.
        /// </summary>
        /// <param name="parent">Parent window.</param>
        /// <param name="i18n">I18n.</param>
        /// <param name="message">Translatable message.</param>
        /// <param name="responseHandler">Delegate method that is called
        /// when the dialog's response event is triggered.</param>
        /// <param name="icon"></param>
        public static void ShowErrorDialog(
            Window parent, I18n i18n, TranslationString message, ResponseHandler responseHandler = null, Pixbuf icon = null)
        {
            Initializer.Initialize(i18n);
            CustomMessageDialog dialog = new CustomMessageDialog(
                i18n, parent, i18n.TrObject(Constants.UiDomain, "Error"), message,
                MessageType.Error, ButtonsType.Ok);
            RunDialog(dialog, responseHandler, icon);
        }

        /// <summary>
        /// Shows an error dialog for an exception.
        /// </summary>
        /// <param name="parent">Parent window.</param>
        /// <param name="i18n">I18n.</param>
        /// <param name="exception">The exception that should be shown.</param>
        /// <param name="title">Translatable dialog title.</param>
        /// <param name="responseHandler">Delegate method that is called
        /// when the dialog's response event is triggered.</param>
        /// <param name="icon">Dialog icon</param>
        public static void ShowErrorDialog(
            Window parent, I18n i18n, Exception exception, TranslationString title = null,
            ResponseHandler responseHandler = null, Pixbuf icon = null)
        {
            Initializer.Initialize(i18n);
            CustomMessageDialog dialog = new CustomMessageDialog(
                i18n, parent,
                title ?? i18n.TrObject(Constants.UiDomain, "Error", exception.GetType().Name),
                i18n.TrObject(Constants.UiDomain, "{0}{1}",
                          exception.Message, Environment.NewLine),
                MessageType.Error,
                ButtonsType.Ok);
            RunDialog(dialog, responseHandler, icon);
        }

        /// <summary>
        /// Shows an information dialog.
        /// </summary>
        /// <param name="parent">Parent window.</param>
        /// <param name="i18n">I18n.</param>
        /// <param name="message">Translatable message.</param>
        /// <param name="responseHandler">Delegate method that is called
        /// when the dialog's response event is triggered.</param>
        /// <param name="icon">Dialog icon</param>
        public static void ShowInfoDialog(
            Window parent, I18n i18n, TranslationString message, ResponseHandler responseHandler = null, Pixbuf icon = null)
        {
            Initializer.Initialize(i18n);
            CustomMessageDialog dialog = new CustomMessageDialog(
                i18n, parent, i18n.TrObject(Constants.UiDomain, "Info"), message,
                MessageType.Info, ButtonsType.Close);
            RunDialog(dialog, responseHandler, icon);
        }

        /// <summary>
        /// Shows a question dialog.
        /// </summary>
        /// <param name="parent">Parent window.</param>
        /// <param name="i18n">I18n.</param>
        /// <param name="message">Translatable message.</param>
        /// <param name="responseHandler">Delegate method that is called
        /// when the dialog's response event is triggered.</param>
        /// <param name="defaultResponse">Default response.</param>
        /// <param name="icon">Dialog icon</param>
        public static void ShowQuestionDialog(
            Window parent, I18n i18n, TranslationString message, ResponseHandler responseHandler,
            ResponseType defaultResponse = ResponseType.No, Pixbuf icon = null)
        {
            Initializer.Initialize(i18n);
            CustomMessageDialog dialog = new CustomMessageDialog(
                i18n, parent, i18n.TrObject(Constants.UiDomain, "Question"), message,
                MessageType.Question, ButtonsType.YesNo);
            dialog.DefaultResponse = defaultResponse;
            RunDialog(dialog, responseHandler, icon);
        }

        /// <summary>
        /// Shows a warning dialog.
        /// </summary>
        /// <param name="parent">Parent window.</param>
        /// <param name="i18n">I18n.</param>
        /// <param name="message">Translatable message.</param>
        /// <param name="responseHandler">Delegate method that is called
        /// when the dialog's response event is triggered.</param>
        /// <param name="icon">Dialog icon</param>
        public static void ShowWarningDialog(
            Window parent, I18n i18n, TranslationString message, ResponseHandler responseHandler, Pixbuf icon = null)
        {
            CustomMessageDialog dialog = new CustomMessageDialog(
                i18n, parent, i18n.TrObject(Constants.UiDomain, "Warning"), message,
                MessageType.Warning, ButtonsType.OkCancel);
            RunDialog(dialog, responseHandler, icon);
        }

        internal static void RunDialog(Dialog dialog, ResponseHandler responseHandler, Pixbuf icon)
        {
            if (icon != null) {
                dialog.Icon = icon;
            }
            if (responseHandler != null) {
                dialog.Response += responseHandler;
            }
            dialog.Run();
            dialog.Destroy();
        }
    }
}

