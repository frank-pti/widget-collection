/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.IO;

namespace Frank.Widgets.Dialogs
{
	/// <summary>
	/// Gtk file dialog.
	/// </summary>
    public abstract class GtkFileDialog: IFileDialog
    {
        private DialogResult dialogResult;

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Dialogs.GtkFileDialog"/> class.
		/// </summary>
        public GtkFileDialog ()
        {
        }

		/// <summary>
		/// Builds the dialog.
		/// </summary>
		/// <returns>The dialog.</returns>
        protected abstract Gtk.FileChooserDialog BuildDialog();

        #region IDialog implementation
        /// <inheritdoc/>
        public virtual DialogResult RunDialog ()
        {
            Gtk.FileChooserDialog dialog = BuildDialog();
            if (FileName != null) {
                dialog.SetFilename(FileName.FullName);
                dialog.CurrentName = FileName.Name;
            }
            dialog.Response += HandleResponse;
            dialog.ShowAll();
            dialog.Run();
            if (!String.IsNullOrWhiteSpace(dialog.Filename)) {
                FileName = new FileInfo(dialog.Filename);
            }
            dialog.Destroy();
            return dialogResult;
        }
        #endregion

		/// <summary>
		/// Handles the response.
		/// </summary>
		/// <param name="o">the sender object.</param>
		/// <param name="args">the event arguments.</param>
        protected virtual void HandleResponse (object o, Gtk.ResponseArgs args)
        {
            switch (args.ResponseId) {
            case Gtk.ResponseType.Accept:
            case Gtk.ResponseType.Ok:
            case Gtk.ResponseType.Apply:
                dialogResult = DialogResult.Ok;
                SelectedFileFilter = (FileFilter)((o as Gtk.FileChooserDialog).Filter);
                break;
            default:
                dialogResult = DialogResult.Cancel;
                break;
            }
        }

        #region IFileDialog implementation
		/// <inheritdoc/>
        public FileInfo FileName {
            get;
            set;
        }
        
		/// <inheritdoc/>
        public IList<FileFilter> FileFilters {
            get;
            set;
        }
        
		/// <inheritdoc/>
        public FileFilter SelectedFileFilter {
            get;
            set;
        }
        #endregion
    }
}

