/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Reflection;

namespace Frank.Widgets
{
    public abstract class SettingsPage : Gtk.VBox
    {
        public delegate void SettingChangedDelegate(object sender, PropertyInfo property);
        public event SettingChangedDelegate SettingsChanged;

        protected SettingsPage()
        {
        }

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (SettingsChanged != null) {
                Type type = this.GetType();
                PropertyInfo property = type.GetProperty(propertyName);
                SettingsChanged(this, property);
            }
        }
    }
}
