/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Gtk;

namespace Frank.Widgets
{
    /// <summary>
    /// Cell renderer that renders an icon or text. The text is rendered if no icon is set.
    /// </summary>
    public class CellRendererIconOrText: CellRenderer
    {
        private class InternalCellRendererText: CellRendererText
        {
            public void StartRender(Gdk.Drawable window, Widget widget, Gdk.Rectangle backgroundArea, Gdk.Rectangle cellArea, Gdk.Rectangle exposeArea, CellRendererState flags)
            {
                Render(window, widget, backgroundArea, cellArea, exposeArea, flags);
            }
        }

        private class InternalCellRendererPixbuf: CellRendererPixbuf
        {
            public void StartRender(Gdk.Drawable window, Widget widget, Gdk.Rectangle backgroundArea, Gdk.Rectangle cellArea, Gdk.Rectangle exposeArea, CellRendererState flags)
            {
                Render(window, widget, backgroundArea, cellArea, exposeArea, flags);
            }
        }

        private InternalCellRendererPixbuf pixbufRenderer;
        private InternalCellRendererText textRenderer;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.CellRendererIconOrText"/> class.
        /// </summary>
        public CellRendererIconOrText ()
        {
            this.pixbufRenderer = new InternalCellRendererPixbuf();
            this.textRenderer = new InternalCellRendererText();
        }

        /// <summary>
        /// Gets or sets the icon that should be rendered.
        /// </summary>
        /// <value>The pixbuf to be rendered.</value>
        [GLib.Property("pixbuf")]
        public Gdk.Pixbuf Pixbuf {
            get {
                return pixbufRenderer.Pixbuf;
            }
            set {
                pixbufRenderer.Pixbuf = value;
            }
        }

        /// <summary>
        /// Gets or sets the text that is rendered if no icon is available
        /// </summary>
        /// <value>The text.</value>
        [GLib.Property("text")]
        public string Text {
            get {
                return textRenderer.Text;
            }
            set {
                textRenderer.Text = value;
            }
        }

        /// <inheritdoc/>
        protected override void Render (Gdk.Drawable window, Widget widget, Gdk.Rectangle backgroundArea, Gdk.Rectangle cellArea, Gdk.Rectangle exposeArea, CellRendererState flags)
        {
            if (pixbufRenderer.Pixbuf != null) {
                pixbufRenderer.StartRender(window, widget, backgroundArea, cellArea, exposeArea, flags);
            } else if (textRenderer.Text != null) {
                textRenderer.StartRender(window, widget, backgroundArea, cellArea, exposeArea, flags);
            } else {
                base.Render (window, widget, backgroundArea, cellArea, exposeArea, flags);
            }
        }

        /// <inheritdoc/>
        public override void GetSize (Widget widget, ref Gdk.Rectangle cellArea, out int xOffset, out int yOffset, out int width, out int height)
        {
            if (pixbufRenderer.Pixbuf != null) {
                pixbufRenderer.GetSize(widget, ref cellArea, out xOffset, out yOffset, out width, out height);
            } else if (textRenderer.Text != null) {
                textRenderer.GetSize(widget, ref cellArea, out xOffset, out yOffset, out width, out height);
            } else {
                base.GetSize (widget, ref cellArea, out xOffset, out yOffset, out width, out height);
            }
        }
    }
}

