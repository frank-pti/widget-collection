/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gtk;

namespace Frank.Widgets
{
    public abstract class TabularSettingsPage : SettingsPage
    {
        private WidgetStack stack;
        private Table table;
        private uint columns;

        protected TabularSettingsPage(uint columns)
        {
            this.columns = columns;
            table = new Table(0, columns, false);
            stack = new WidgetStack(table);
            Add(stack);
        }

        protected void AttachTableRow(uint row, params Widget[] widgets)
        {
            AttachTableRow(table, row, widgets);
        }

        public static void AttachTableRow(Table table, uint row, params Widget[] widgets)
        {
            for (uint leftAttach = 0; leftAttach < widgets.Length; leftAttach++) {
                uint rightAttach = leftAttach + 1;
                if (rightAttach == widgets.Length && rightAttach < table.NColumns) {
                    rightAttach = table.NColumns;
                }
                table.Attach(widgets[leftAttach], leftAttach, rightAttach, row, row + 1,
                             AttachOptions.Fill, AttachOptions.Fill, 3, 3);
            }
        }
    }
}

