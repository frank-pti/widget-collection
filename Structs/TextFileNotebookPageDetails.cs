/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Reflection;
using Internationalization;

namespace Frank.Widgets.Structs
{
	/// <summary>
	/// Text file notebook page details.
	/// </summary>
    public struct TextFileNotebookPageDetails
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Structs.TextFileNotebookPageDetails"/> struct.
		/// </summary>
		/// <param name="title">Title.</param>
		/// <param name="path">Path.</param>
		/// <param name="isResource">If set to <c>true</c> is resource.</param>
        public TextFileNotebookPageDetails(TranslationString title, string path, bool isResource):
            this(title, path, isResource, null)
        {
        }

		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Structs.TextFileNotebookPageDetails"/> struct.
		/// </summary>
		/// <param name="title">Title.</param>
		/// <param name="path">Path.</param>
		/// <param name="isResource">If set to <c>true</c> is resource.</param>
		/// <param name="assembly">Assembly.</param>
        public TextFileNotebookPageDetails(TranslationString title, string path, bool isResource, Assembly assembly):
            this()
        {
            Title = title;
            Path = path;
            IsResource = isResource;
            Assembly = assembly;
        }

		/// <summary>
		/// Gets the title.
		/// </summary>
		/// <value>The title.</value>
        public TranslationString Title {
            get;
            private set;
        }

		/// <summary>
		/// Gets the path.
		/// </summary>
		/// <value>The path.</value>
        public string Path {
            get;
            private set;
        }

		/// <summary>
		/// Gets a value indicating whether this instance is resource.
		/// </summary>
		/// <value><c>true</c> if this instance is resource; otherwise, <c>false</c>.</value>
        public bool IsResource {
            get;
            private set;
        }

		/// <summary>
		/// Gets the assembly.
		/// </summary>
		/// <value>The assembly.</value>
        public Assembly Assembly {
            get;
            private set;
        }
    }
}

