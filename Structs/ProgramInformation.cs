/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Widgets.Structs
{
	/// <summary>
	/// Program information.
	/// </summary>
    public struct ProgramInformation
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Structs.ProgramInformation"/> struct.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="version">Version.</param>
		/// <param name="website">Website.</param>
        public ProgramInformation(string name, string version, string website):
            this()
        {
            Name = name;
            Version = version;
            Website = website;
        }

		/// <summary>
		/// Gets the name.
		/// </summary>
		/// <value>The name.</value>
        public string Name {
            get;
            private set;
        }

		/// <summary>
		/// Gets the version.
		/// </summary>
		/// <value>The version.</value>
        public string Version {
            get;
            private set;
        }

		/// <summary>
		/// Gets the website.
		/// </summary>
		/// <value>The website.</value>
        public string Website {
            get;
            private set;
        }
    }
}

