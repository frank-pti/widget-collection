/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace Frank.Widgets.Structs
{
	/// <summary>
	/// Developer information.
	/// </summary>
    public struct DeveloperInformation
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="Frank.Widgets.Structs.DeveloperInformation"/> struct.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="address">Address.</param>
		/// <param name="place">Place.</param>
		/// <param name="country">Country.</param>
		/// <param name="telephone">Telephone.</param>
		/// <param name="website">Website.</param>
        public DeveloperInformation(string name, string address, string place, string country, string telephone, string website):
            this()
        {
            Name = name;
            Address = address;
            Place = place;
            Country = country;
            Telephone = telephone;
            Website = website;
        }

		/// <summary>
		/// Gets the name.
		/// </summary>
		/// <value>The name.</value>
        public string Name {
            get;
            private set;
        }

		/// <summary>
		/// Gets the address.
		/// </summary>
		/// <value>The address.</value>
        public string Address {
            get;
            private set;
        }

		/// <summary>
		/// Gets the place.
		/// </summary>
		/// <value>The place.</value>
        public string Place {
            get;
            private set;
        }

		/// <summary>
		/// Gets the country.
		/// </summary>
		/// <value>The country.</value>
        public string Country {
            get;
            private set;
        }

		/// <summary>
		/// Gets the telephone.
		/// </summary>
		/// <value>The telephone.</value>
        public string Telephone {
            get;
            private set;
        }

		/// <summary>
		/// Gets the website.
		/// </summary>
		/// <value>The website.</value>
        public string Website {
            get;
            private set;
        }
    }
}

