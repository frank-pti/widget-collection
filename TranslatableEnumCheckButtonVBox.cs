/*
 * A collection of Gtk# widgets
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using InternationalizationUi;
using System.Collections.Generic;
using Internationalization;
using System.Reflection;
using FrankPti.Collections;

namespace Frank.Widgets
{
    /// <summary>
    /// Widget that displays a list of check buttons, which names are read from an enum.
    /// </summary>
    public class TranslatableEnumCheckButtonVBox<TEnum>: Gtk.VBox
        where TEnum: struct
    {
        /// <summary>Delegate method for ToggledEvent.</summary>
        public delegate void ToggledDelegate(TEnum entry, bool value);
        /// <summary>
        /// Occurs when toggled.
        /// </summary>
        public event ToggledDelegate Toggled;

        private IDictionary<TEnum, TranslatableCheckButton> buttonsByEnum;
        private IDictionary<TranslatableCheckButton, TEnum> buttons;

        /// <summary>
        /// Initializes a new instance of the <see cref="Frank.Widgets.TranslatableEnumCheckButtonVBox`1"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        public TranslatableEnumCheckButtonVBox(I18n i18n)
        {
            buttonsByEnum = new Dictionary<TEnum, TranslatableCheckButton> ();
            buttons = new Dictionary<TranslatableCheckButton, TEnum> ();
            TEnum[] values = Enum.GetValues (typeof(TEnum)) as TEnum[];
            foreach (TEnum value in values)
            {
                TranslatableCheckButton checkButton = new TranslatableCheckButton(GetTranslatableCaption(i18n, value));
                PackStart (checkButton);
                checkButton.Toggled += HandleToggled;
                buttonsByEnum.Add (value, checkButton);
                buttons.Add (checkButton, value);
            }
        }

        private  void HandleToggled (object sender, EventArgs e)
        {
            if (Toggled != null) {
                TranslatableCheckButton button = (TranslatableCheckButton)sender;
                Toggled(buttons[button], button.Active);
            }
        }

        /// <summary>
        /// Gets or sets the active.
        /// </summary>
        /// <value>The active.</value>
        public EnumDictionary<TEnum, bool> Active {
            get {
                EnumDictionary<TEnum, bool> values = new EnumDictionary<TEnum, bool>();
                foreach (TEnum key in buttonsByEnum.Keys) {
                    values[key] = buttonsByEnum[key].Active && buttonsByEnum[key].Sensitive;
                }
                return values;
            }
            set {
                foreach (TEnum key in buttonsByEnum.Keys) {
                    if (value != null && value.ContainsKey (key)) {
                        buttonsByEnum[key].Active = value[key];
                    } else {
                        buttonsByEnum[key].Active = false;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the sensitive entries.
        /// </summary>
        /// <value>The sensitive entries.</value>
        public IDictionary<TEnum, bool> SensitiveEntries {
            get {
                Dictionary<TEnum, bool> sensitive = new Dictionary<TEnum, bool>();
                foreach (TEnum key in buttonsByEnum.Keys) {
                    sensitive.Add (key, buttonsByEnum[key].Sensitive);
                }
                return sensitive;
            }
            set {
                foreach (TEnum key in value.Keys) {
                    buttonsByEnum[key].Sensitive = value[key];
                }
            }
        }
        
        private static TranslationString GetTranslatableCaption(I18n i18n, TEnum val)
        {
            string enumName = Enum.GetName (typeof(TEnum), val);
            FieldInfo fieldInfo = val.GetType().GetField(enumName);
            TranslatableCaptionAttribute[] attributes = 
                fieldInfo.GetCustomAttributes(typeof(TranslatableCaptionAttribute), false)
                    as TranslatableCaptionAttribute[];
            if (attributes.Length == 0) {
                throw new ArgumentException(String.Format (
                    "Value {0} of enum type {1} has no TranslatableCaption attribute", enumName, typeof(TEnum).FullName));
            }
            return attributes[0].GetTranslation(i18n);
        }
    }
}

